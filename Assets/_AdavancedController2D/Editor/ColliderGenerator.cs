﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using HrtzzUtilities;

namespace Saya.Level.Editor
{
    public class ColliderGenerator : EditorWindow
    {
        float colliderSkin = 0.1f;
        bool topCollider = true;
        bool bottomCollider = true;
        bool rightCollider = true;
        bool leftCollider = true;

        // bool showColors = false;

        // TODO Save Color Settings to scriptable object.
        Color topColor = Color.green;
        Color bottomColor = Color.cyan;
        Color rightColor = Color.yellow;
        Color leftColor = Color.yellow;

        GameObject colliderPrefab;
        GameObject tempObject = null;
        SpriteRenderer spriteRenderer;

        Vector2 boundsSize;

        [MenuItem("Utilities/Collider Generator")]
        static void InitWindow()
        {
            ColliderGenerator colliderGenerator = GetWindow<ColliderGenerator>("Collider Generator");
            colliderGenerator.minSize = new Vector2(200, 200);
        }

        void OnGUI()
        {
            GUILayout.Space(5);
            GUILayout.Label("Settings", EditorStyles.boldLabel);
            colliderSkin = EditorGUILayout.Slider("Collider Skin", colliderSkin, 0.01f, 0.2f);

            GUILayout.BeginVertical("box");
            GUILayout.Label("Colliders", EditorStyles.boldLabel);
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.BeginVertical();
            topCollider = EditorGUILayout.Toggle("Top Collider", topCollider);
            bottomCollider = EditorGUILayout.Toggle("Bottom Collider", bottomCollider);
            rightCollider = EditorGUILayout.Toggle("Right Collider", rightCollider);
            leftCollider = EditorGUILayout.Toggle("Left Collider", leftCollider);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();

            //GUILayout.BeginVertical("box");
            //showColors = EditorGUILayout.Foldout(showColors, "Colors");
            //if (showColors)
            //{
            //    GUILayout.BeginHorizontal();
            //    GUILayout.Space(10);
            //    GUILayout.BeginVertical();
            //    topColor = EditorGUILayout.ColorField("TopColor", topColor);
            //    bottomColor = EditorGUILayout.ColorField("BottomColor", bottomColor);
            //    rightColor = EditorGUILayout.ColorField("RightColor", rightColor);
            //    leftColor = EditorGUILayout.ColorField("LeftColor", leftColor);
            //    GUILayout.EndHorizontal();
            //    GUILayout.EndVertical();
            //}
            //GUILayout.EndVertical();

            if (GUILayout.Button("Show/Hide Colliders", GUILayout.Height(30)))
            {
                ToggleCollidersColor();
            }

            if (GUILayout.Button("Generate Colliders", GUILayout.Height(30)))
            {
                if (!colliderPrefab)
                {
                    Debug.LogError("Reference to collider prefab is missing");
                }
                else
                {
                    GenerateColliders();
                }
            }
        }

        void Awake()
        {
            if (!colliderPrefab)
            {
                colliderPrefab =
                    AssetDatabase.LoadAssetAtPath<GameObject>(
                        "Assets/_AdavancedController2D/Prefabs/GenericCollider.prefab");
            }
        }

        void ToggleCollidersColor()
        {
            GroundCollider[] groundColliders = FindObjectsOfType<GroundCollider>();
            if (groundColliders.Length > 0)
            {
                foreach (var gc in groundColliders)
                {
                    gc.ToggleSpriteState();
                }
            }
        }

        void GenerateColliders()
        {
            foreach (GameObject go in Selection.gameObjects)
            {
                Ground ground = go.GetComponent<Ground>();
                if (ground)
                {
                    // Destroy previous colliders.
                    List<Transform> bin = go.transform.Cast<Transform>().ToList();
                    foreach (var garbage in bin.Where(garbage => garbage.GetComponent<GroundCollider>()))
                    {
                        DestroyImmediate(garbage.gameObject);
                    }

                    boundsSize = ground.SpriteRenderer.bounds.size;
                    if (topCollider)
                    {
                        GenerateCollisionObect(go.transform, ColliderType.Top);
                    }

                    if (bottomCollider)
                    {
                        GenerateCollisionObect(go.transform, ColliderType.Bottom);
                    }

                    if (rightCollider)
                    {
                        GenerateCollisionObect(go.transform, ColliderType.Right);
                    }

                    if (leftCollider)
                    {
                        GenerateCollisionObect(go.transform, ColliderType.Left);
                    }
                }
                else
                {
                    Debug.LogWarning("The selected gameobject " + go.name + " is not a ground.");
                }
            }
        }

        void GenerateCollisionObect(Transform parentTransform, ColliderType colliderType)
        {
            tempObject = Instantiate(colliderPrefab);
            tempObject.name = colliderType.ToString();

            switch (colliderType)
            {
                case ColliderType.Top:
                    tempObject.transform.localScale = new Vector2(boundsSize.x, colliderSkin);
                    tempObject.transform.position = (Vector2) (parentTransform.position) +
                                                    Vector2.up * ((boundsSize.y - colliderSkin) * 0.5f);
                    tempObject.layer = Layers.GroundLayer;
                    tempObject.GetComponent<GroundCollider>().SetColliderColor(topColor);
                    break;
                case ColliderType.Bottom:
                    tempObject.transform.localScale = new Vector2(boundsSize.x - colliderSkin, colliderSkin);
                    tempObject.transform.position = (Vector2) (parentTransform.position) +
                                                    Vector2.down * ((boundsSize.y - colliderSkin) * 0.5f);
                    tempObject.layer = Layers.GroundLayer;
                    tempObject.GetComponent<GroundCollider>().SetColliderColor(bottomColor);
                    break;
                case ColliderType.Right:
                    tempObject.transform.localScale = new Vector2(colliderSkin, boundsSize.y - colliderSkin);
                    tempObject.transform.position = (Vector2) (parentTransform.position) +
                                                    Vector2.right * ((boundsSize.x - colliderSkin) * 0.5f);
                    tempObject.layer = Layers.WallLayer;
                    tempObject.GetComponent<GroundCollider>().SetColliderColor(rightColor);
                    break;
                case ColliderType.Left:
                    tempObject.transform.localScale = new Vector2(colliderSkin, boundsSize.y - colliderSkin);
                    tempObject.transform.position = (Vector2) (parentTransform.position) +
                                                    Vector2.left * ((boundsSize.x - colliderSkin) * 0.5f);
                    tempObject.layer = Layers.WallLayer;
                    tempObject.GetComponent<GroundCollider>().SetColliderColor(leftColor);
                    break;
                default:
                    break;
            }

            tempObject.transform.SetParent(parentTransform);
            tempObject = null;
        }

        public enum ColliderType
        {
            Top,
            Bottom,
            Right,
            Left
        }
    }
}