﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Species : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public SpeciesType speciesType = SpeciesType.Black;
    public Sprite blackSprite;
    public Sprite whiteSprite;

    public void ToggleSpeciesColor()
    {
        if (spriteRenderer)
        {
            switch (speciesType)
            {
                case SpeciesType.Black:
                    spriteRenderer.sprite = whiteSprite;
                    break;
                case SpeciesType.White:
                    spriteRenderer.sprite = blackSprite;
                    break;
            }
        }
    }
}
