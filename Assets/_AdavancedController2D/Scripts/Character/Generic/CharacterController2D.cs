﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using HrtzzUtilities;
using UnityEditor;
using UnityEngine;

namespace Saya
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    public class CharacterController2D : MonoBehaviour
    {
        public LayerMask groundLayer;
        public float groundCheckLength = 0.1f;


        private Vector2 m_CurrentPosition;
        private Vector2 m_PreviousPosition;
        private Vector2 m_MoveVector;

        private RaycastHit2D[] m_HitBuffer = new RaycastHit2D[3]; // TODO : Need to utilize for higher accuracy. 
        private RaycastHit2D[] m_FoundHits = new RaycastHit2D[3]; // TODO : Need to utilize for higher accuracy .
        private Collider2D[] m_GroundColliders = new Collider2D[3]; // TODO : Need to utilize for higher accuracy. 
        private Vector2[] m_RaycastPositions = new Vector2[3];
        private Vector2 m_RayCenterPos;
        private Vector2 m_RayDirection;
        private ContactFilter2D m_ContactFilter;

        private Bounds m_ColliderBounds;

        public Rigidbody2D Rb { get; private set; }
        public BoxCollider2D Coll { get; private set; }
        public Vector2 Velocity { get; private set; }
        public bool IsGrounded { get; private set; }
        public bool IsCeilinged { get; private set; }

        private void Awake()
        {
            Rb = GetComponent<Rigidbody2D>();
            Coll = GetComponent<BoxCollider2D>();
            m_ColliderBounds = Coll.bounds;
            m_ContactFilter.layerMask = groundLayer;
            m_ContactFilter.useLayerMask = true;
            m_ContactFilter.useTriggers = false;
        }

        private void Start()
        {
            m_CurrentPosition = m_PreviousPosition = Rb.position;
            m_MoveVector = Vector2.zero;
        }

        private void FixedUpdate()
        {
            m_PreviousPosition = Rb.position;
            m_CurrentPosition = m_PreviousPosition + m_MoveVector;
            Velocity = (m_CurrentPosition - m_PreviousPosition) / Time.deltaTime;
            Rb.MovePosition(m_CurrentPosition);
            m_MoveVector = Vector2.zero;

            CheckCollisions(false);
            CheckCollisions();
        }

        public void Move(Vector2 movement)
        {
            m_MoveVector += movement;
        }

        public void Teleport(Vector2 destination)
        {
            Vector2 delta = destination - m_CurrentPosition;
            m_PreviousPosition += delta;
            m_CurrentPosition = destination;
            Rb.MovePosition(destination);
        }

        private void CheckCollisions(bool bottom = true)
        {
            Vector2 rayDirection;
            if (bottom)
            {
                IsGrounded = false;
                rayDirection = Vector2.down;
            }
            else
            {
                IsCeilinged = false;
                rayDirection = Vector2.up;
            }

            m_RayCenterPos = (Vector2) Coll.bounds.center + rayDirection * ((m_ColliderBounds.size.y - groundCheckLength) / 2);
            m_RaycastPositions[0] = m_RayCenterPos + Vector2.left * m_ColliderBounds.size.x / 2;
            m_RaycastPositions[1] = m_RayCenterPos;
            m_RaycastPositions[2] = m_RayCenterPos + Vector2.right * m_ColliderBounds.size.x / 2;


            for (int i = 0; i < 3; i++)
            {
                int hitCount = Physics2D.RaycastNonAlloc(m_RaycastPositions[i], rayDirection, m_HitBuffer, groundCheckLength, groundLayer);

                if (hitCount > 0)
                {
                    if (bottom)
                    {
                        m_FoundHits[i] = hitCount > 0 ? m_HitBuffer[0] : new RaycastHit2D();
                        m_GroundColliders[i] = m_FoundHits[i].collider;
                        IsGrounded = true;
                    }
                    else
                    {
                        IsCeilinged = true;
                        
                        /*
                        for (int j = 0; j < m_HitBuffer.Length; j++)
                        {
                            if (m_HitBuffer[j].collider != null)
                            {
                                IsCeilinged = true;
                                break;
                            }
                        }
                        */
                    }
                }
            }
        }
    }
}