﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

namespace Saya.Controller2D
{
    public class PlayerLook : MonoBehaviour
    {
        [SerializeField]
        [Range(0, 5)]
        private float lookThreshold = 2;

        private Vector2 defaultCameraOffset;

        private ProCamera2D proCamera;
        private PlayerMotor playerMotor;
        private float refFloat;

        private void Awake()
        {
            playerMotor = GetComponent<PlayerMotor>();
        }

        private void OnEnable()
        {
            if (playerMotor)
            {
                playerMotor.OnFixedUpdateEvent += ManagedFixedUpdate;
            }
        }

        private void OnDisable()
        {
            if (playerMotor)
            {
                playerMotor.OnFixedUpdateEvent -= ManagedFixedUpdate;
            }
        }
        // Start is called before the first frame update
        private void Start()
        {
            proCamera = ProCamera2D.Instance;
            //defaultCameraOffset = proCamera.OverallOffset;
        }

        // Update is called once per frame
        private void ManagedFixedUpdate()
        {
            float target = defaultCameraOffset.y;

            if (playerMotor.DirectionalInput.x == 0 && playerMotor.IsGrounded && !playerMotor.IsBashing)
            {
                //target = Mathf.SmoothDamp(proCamera.OverallOffset.y, defaultCameraOffset.y + lookThreshold * playerMotor.RightStickInput.y, ref refFloat, 0.05f);
            }

            //if (proCamera.OverallOffset.y != target)
            //{
            //    proCamera.OverallOffset = new Vector2(defaultCameraOffset.x, target);
            //}
        }


    }
}
