﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using HrtzzUtilities;

namespace Saya.Controller2D
{
    public class PlayerJump : MonoBehaviour
    {
        [SerializeField] private float maxJumpHeight = 5.0f;
        [SerializeField] private float doubleJumpHeight = 5.0f;

        [SerializeField] private float fallMultiplier = 2.5f;
        [SerializeField] private float lowFallMultiplier = 2.0f;
        [SerializeField] private int maxJumpAllowed = 2;

        private int jumpCount = 0;
        private bool isInitialJump = false;

        private float groundJumpVelocity = 0; // will be calculated based on height.
        private float doubleJumpVelocity = 0;
        private bool isAllowedToJump = false;
        private bool isJumpButtonDown = false;

        private PlayerMotor mPlayerMotor;

        //public bool isAllowedToJump { get => isAllowedToJump; set => isAllowedToJump = value; }

        // Start is called before the first frame update
        private void Start()
        {
            groundJumpVelocity = Mathf.Sqrt(maxJumpHeight * 2 * -Physics2D.gravity.y);
            doubleJumpVelocity = Mathf.Sqrt(doubleJumpHeight * 2 * -Physics2D.gravity.y);
            jumpCount = maxJumpAllowed;

            mPlayerMotor = GetComponent<PlayerMotor>();
        }

        private void OnEnable()
        {
            if (!mPlayerMotor)
            {
                mPlayerMotor = GetComponent<PlayerMotor>();
            }

            mPlayerMotor.OnFixedUpdateEvent += ManagedFixedUpdate;
            mPlayerMotor.JumpButtonPressedEvent += JumpPressed;
            mPlayerMotor.JumpButtonReleasedEvent += JumpReleased;
            mPlayerMotor.PlayerLandedEvent += ResetJumpVariables;
            mPlayerMotor.DashStateChangedEvent += SetJumpAllowedState;
        }

        private void OnDisable()
        {
            if (mPlayerMotor)
            {
                mPlayerMotor.OnFixedUpdateEvent -= ManagedFixedUpdate;
                mPlayerMotor.JumpButtonPressedEvent -= JumpPressed;
                mPlayerMotor.JumpButtonReleasedEvent -= JumpReleased;
                mPlayerMotor.PlayerLandedEvent -= ResetJumpVariables;
                mPlayerMotor.DashStateChangedEvent -= SetJumpAllowedState;
            }
        }

        private void ManagedFixedUpdate()
        {
            if (mPlayerMotor.IsDashing || mPlayerMotor.IsBashing)
            {
                return;
            }

            if (isAllowedToJump)
            {
                if (mPlayerMotor.IsGrounded || jumpCount > 1)
                {
                    mPlayerMotor.WasGrounded = false;
                    mPlayerMotor.SetVelocityY(0);

                    if (mPlayerMotor.IsGrounded)
                    {
                        isInitialJump = true; // Set initial jump for applying variable jump forces.
                    }
                    else
                    {
                        jumpCount--;
                    }

                    //TODO : Modify to work with custom gravity.
//                    mPlayerMotor.MoveVector.y = isInitialJump ? groundJumpVelocity : doubleJumpVelocity;
                    mPlayerMotor.Rb.AddForce(Vector2.up * (isInitialJump ? groundJumpVelocity : doubleJumpVelocity), ForceMode2D.Impulse);
                    isInitialJump = false;
                }

                isAllowedToJump = false;
            }

            // For Better Jump mechanics, alters the gravity scale of characters Rigidbody to make it fall faster.
            if (mPlayerMotor.Rb.velocity.y < 0)
            {
                mPlayerMotor.Rb.gravityScale = fallMultiplier;
            }
            else if (mPlayerMotor.Rb.velocity.y > 0 && !isJumpButtonDown)
            {
                mPlayerMotor.Rb.gravityScale = lowFallMultiplier;
            }
            else
            {
                mPlayerMotor.Rb.gravityScale = 1.0f;
            }
        }

        private void SetJumpAllowedState(bool state)
        {
            isAllowedToJump = state;
        }

        private void JumpPressed()
        {
            if (!mPlayerMotor.IsDashing)
            {
                isAllowedToJump = true;
                isJumpButtonDown = true;
            }
        }

        private void JumpReleased()
        {
            isJumpButtonDown = false;
        }

        private void ResetJumpVariables()
        {
            if (!mPlayerMotor.IsDashing)
            {
                jumpCount = maxJumpAllowed;
            }
        }
    }
}