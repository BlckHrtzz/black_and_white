﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;
using HrtzzUtilities;
using Saya.DamageSystem;

namespace Saya.Controller2D
{
    public enum PlayerState
    {
        Idle,
        Walking,
        Sprinting,
        Jumping,
        Dashing,
        WallSliding
    }

    public enum FaceDirection
    {
        Right,
        Left
    }

    [DisallowMultipleComponent]
    [RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
    public class PlayerMotor : MonoBehaviour
    {
        public Vector2 MoveVector;

        #region Private Variables

        private ProCamera2D m_ProCamera2D;
        private Rigidbody2D m_Rigidbody;
        private Collider2D m_BoxCollider;
        private SpriteRenderer m_PlayerGraphic;

        private float m_GroundMovementFactor = 1.0f;

        [Range(0, 2)] [SerializeField] private float airMovementFactor = 0.7f;


        [Space] [SerializeField] private LayerMask groundLayer = Layers.Default; // Ground Layer mask

        [SerializeField] [Range(0.02f, 0.1f)] private float mGroundCheckSkin = 0.15f; // Radius of the sphere which will be used to check ground collisions.

        [SerializeField] private Transform groundCheck;

        private Vector2 groundCheckBoxSize;
        private Vector2 groundOffset;
        private Bounds colliderBounds;


        private bool mIsGrounded = true;
        private bool mWasGrounded = true;
        private bool mOnPlatform = false;

        #endregion

        #region Properties

        public Rigidbody2D Rb
        {
            get
            {
                if (!m_Rigidbody)
                {
                    m_Rigidbody = GetComponent<Rigidbody2D>();
                }

                return m_Rigidbody;
            }
        }

        public SpriteRenderer PlayerGraphic
        {
            get
            {
                if (!m_PlayerGraphic)
                {
                    m_PlayerGraphic = transform.Find("PlayerGraphic").GetComponent<SpriteRenderer>();
                }

                return m_PlayerGraphic;
            }
        }

        public bool IsFacingRight { get; private set; } = true;

        public Animator CharacterAnimator { get; private set; }

        // Property for directional input.
        public Vector2 DirectionalInput { get; private set; }
        public Vector2 RightStickInput { get; private set; }

        // Property for mIsGrounded;
        public bool IsGrounded
        {
            get => mIsGrounded;
        }

        public bool WasGrounded
        {
            get => mWasGrounded;
            set => mWasGrounded = value;
        }

        public bool IsDashing { get; private set; } = false;
        public bool IsSprintDown { get; private set; } = false;
        public bool IsBashing { get; private set; } = false;

        public Transform FrontCheckTop { get; private set; }
        public Transform FrontCheckBottom { get; private set; }
        public Transform GroundCheck { get; private set; }
        public Collider2D Collider { get; private set; }

        #endregion

        #region Events

        public delegate void KillSomeone();
        public event KillSomeone onkill;

        public event Action KiillSomeone;

        public event Action JumpButtonPressedEvent;
        public event Action JumpButtonReleasedEvent;
        public event Action<bool> SprintStateChangedEvent;
        public event Action DashButtonPressedEvent;

        public event Action OnFixedUpdateEvent;
        public event Action PlayerLandedEvent;
        public event Action<bool> DashStateChangedEvent;
        public event Action<bool> BashButtonStateChangedEvent;
        public event Action BaseAttackPressed;

        #endregion

        #region UnityCallbacks

        private void Awake()
        {
        }

        private void Start()
        {
            m_ProCamera2D = ProCamera2D.Instance;
            Collider = GetComponent<BoxCollider2D>();
            if (Collider)
            {
                colliderBounds = Collider.bounds;
            }
            else
            {
                Debug.LogError("Collider is missing");
            }

            CharacterAnimator = GetComponent<Animator>();

            // Set the default direction.
            IsFacingRight = PlayerGraphic.transform.localScale.x > 0 ? true : false;

            m_PlayerGraphic = transform.Find("PlayerGraphic").GetComponent<SpriteRenderer>();

            CreateCollisionCheckTransfroms();
            UpdateCollisionCheckPointPositions();

            groundOffset = Vector2.down * mGroundCheckSkin * 0.5f;

            if (Collider)
            {
                groundCheckBoxSize = new Vector2(colliderBounds.size.x - mGroundCheckSkin, mGroundCheckSkin);
            }

            SpawnPlayer();
        }

        public void Tick()
        {
            UpdateGroundedState();
        }

        public void FixedTick()
        {
            OnFixedUpdateEvent?.Invoke();
        }

        #endregion

        #region UserDefined Functions

        #region InputCallbacks

        internal void SetDirectionalInput(Vector2 inputValue)
        {
            DirectionalInput = new Vector2(Mathf.Round(inputValue.x), Mathf.Round(inputValue.y));

            // Run only if player is not dashing.
            if (!IsDashing)
            {
                if ((IsFacingRight && DirectionalInput.x < 0) || (!IsFacingRight && DirectionalInput.x > 0))
                // Flip the Player Graphic if the if inputDirection and PlayerGraphic scale do not correspond.
                {
                    Flip();
                }
            }
        }

        internal void OnBashButtonStateChanged(bool shouldBash)
        {
            BashButtonStateChangedEvent?.Invoke(shouldBash);
        }

        internal void SetCameraInput(Vector2 value)
        {
            RightStickInput = value;
        }

        internal void OnJumpPressed()
        {
            JumpButtonPressedEvent?.Invoke();
        }

        internal void OnJumpReleased()
        {
            JumpButtonReleasedEvent?.Invoke();
        }

        internal void OnSprintStateChanged(bool shouldSprint)
        {
            IsSprintDown = shouldSprint;
            SprintStateChangedEvent?.Invoke(IsSprintDown);
        }

        internal void OnDashButtonPressed()
        {
            DashButtonPressedEvent?.Invoke();
        }

        internal void OnBaseAttackPressed()
        {
            BaseAttackPressed?.Invoke();
        }

        #endregion

        private void CreateCollisionCheckTransfroms()
        {
            // TODO : Need to implement in modular manner.
            GroundCheck = new GameObject("GroundCheck").transform;
            FrontCheckTop = new GameObject("FrontCheckTop").transform;
            FrontCheckBottom = new GameObject("FrontCheckBottom").transform;
            GroundCheck.SetParent(transform);
            FrontCheckTop.SetParent(transform);
            FrontCheckBottom.SetParent(transform);

            GroundCheck.localPosition = Vector2.zero;
        }

        private void UpdateCollisionCheckPointPositions()
        {
            float _faceDirection = IsFacingRight ? 1 : -1;
            if (FrontCheckTop)
            {
                var position = transform.position;
                FrontCheckTop.position = new Vector2(position.x + (colliderBounds.size.x * 0.5f * _faceDirection),
                    position.y + colliderBounds.size.y);
            }

            if (FrontCheckBottom)
            {
                FrontCheckBottom.position = new Vector2(
                    transform.position.x + (colliderBounds.size.x * 0.5f * _faceDirection) +
                    mGroundCheckSkin * _faceDirection,
                    transform.position.y + mGroundCheckSkin * 2);
            }
        }

        // TODO : Does this function really belongs here?
        public void OnHurt(Damager damager, Damageable damageable)
        {
            //ProCamera2DShake.Instance.ShakeUsingPreset("EnemyHit");
            //HrtzzLogger.Log("Got Hurt");
            if (damager.forceRespawn)
            {
                SpawnPlayer();
            }
        }

        public void SpawnPlayer(bool resetStats = false)
        {
            // TODO : Improvise functionality for game over or dead cases.
            Rb.position = SceneController.Instance.CurrentCheckPoint.Position;
        }

        public void SetVelocityX(float value)
        {
            m_Rigidbody.velocity = new Vector2(value, m_Rigidbody.velocity.y);
        }

        public void SetVelocityY(float value)
        {
            m_Rigidbody.velocity = new Vector2(m_Rigidbody.velocity.x, value);
        }

        // returns the movement factor for player based on the grounded state.
        public float GetMovementFactor()
        {
            if (!mIsGrounded)
            {
                return airMovementFactor;
            }

            return m_GroundMovementFactor;
        }

        // Checks if the player is touching the ground and updates the mIsGrounded.
        public void UpdateGroundedState()
        {
            if (!GroundCheck)
            {
                GroundCheck = transform.Find("GroundCheck");
            }

            mIsGrounded = Physics2D.OverlapBox((Vector2)GroundCheck.position + groundOffset, groundCheckBoxSize, 0f, groundLayer);

            // Invoke the player landed event.
            if (mIsGrounded && !mWasGrounded)
            {
                mWasGrounded = true;
                PlayerLandedEvent?.Invoke();
            }
        }

        // Change the scale of Player Graphic if required and collision points position as well.
        private void Flip()
        {
            IsFacingRight = !IsFacingRight;
            PlayerGraphic.flipX = !IsFacingRight;
            UpdateCollisionCheckPointPositions();
        }

        // Update the local dash variable.
        public void Dash(bool dashState)
        {
            IsDashing = dashState;
            DashStateChangedEvent?.Invoke(dashState);
            if (dashState)
            {
                SetVelocityY(0);
            }
        }

        public void Bash(bool bashState)
        {
            IsBashing = bashState;
        }

        #endregion

        #region DebugFunctions

        #endregion
    }
}