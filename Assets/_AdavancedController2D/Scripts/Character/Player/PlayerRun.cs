﻿using System.Collections;
using System.Collections.Generic;
using HrtzzUtilities;
using UnityEngine;

namespace Saya.Controller2D
{
    internal enum MovementMode
    {
        VelocityChange,
        ForceChange
    };

    [RequireComponent(typeof(PlayerMotor))]
    public class PlayerRun : MonoBehaviour
    {
        private PlayerMotor mPlayerMotor; // Reference to PlayerMotor class.

        [SerializeField] private MovementMode movementMode = MovementMode.VelocityChange;

        [Tooltip("Max Run Speed of the player.")] [SerializeField]
        private float runSpeed = 10f;

        [Tooltip("Max Walk Speed of the player.")] [SerializeField]
        private float walkSpeed = 5f;

        [Tooltip("Smoothing Time.")] [SerializeField]
        private float mSmoothTime = 0.05f;

        [SerializeField] [Tooltip("Works only with Force Change Movement Mode")]
        private float movementForce = 50.0f;

        private float speed;

        [SerializeField] private bool bHoldToSprint = true;

        private Vector2 mRefVelocity = Vector2.zero;
        private Vector2 mTargetVelocity = Vector2.zero;

        // Start is called before the first frame update.
        private void Awake()
        {
            mPlayerMotor = GetComponent<PlayerMotor>();
            SelectSpeed();
        }

        private void OnEnable()
        {
            if (mPlayerMotor)
            {
                mPlayerMotor.OnFixedUpdateEvent += ManagedFixedUpdate;
                mPlayerMotor.SprintStateChangedEvent += SelectSpeed;
                mPlayerMotor.PlayerLandedEvent += UpdateSprintStateOnLand;
            }
        }

        private void OnDisable()
        {
            if (mPlayerMotor)
            {
                mPlayerMotor.OnFixedUpdateEvent -= ManagedFixedUpdate;
                mPlayerMotor.SprintStateChangedEvent -= SelectSpeed;
                mPlayerMotor.PlayerLandedEvent -= UpdateSprintStateOnLand;
            }
        }

        // Update is called once per frame.
        private void ManagedFixedUpdate()
        {
            if (mPlayerMotor.IsDashing || mPlayerMotor.IsBashing)
            {
                return;
            }

            if (movementMode == MovementMode.VelocityChange)
            {
                mTargetVelocity = new Vector2(mPlayerMotor.DirectionalInput.x * speed * mPlayerMotor.GetMovementFactor(), mPlayerMotor.Rb.velocity.y);
                mPlayerMotor.Rb.velocity = Vector2.SmoothDamp(mPlayerMotor.Rb.velocity, mTargetVelocity, ref mRefVelocity, mSmoothTime, Mathf.Infinity, Time.smoothDeltaTime);
            }

            else if (movementMode == MovementMode.ForceChange)
            {
                if (mPlayerMotor.DirectionalInput.x * mPlayerMotor.Rb.velocity.x < speed)
                {
                    mPlayerMotor.Rb.AddForce(Vector2.right * mPlayerMotor.DirectionalInput.x * movementForce * mPlayerMotor.GetMovementFactor());
                }
                else if (Mathf.Abs(mPlayerMotor.Rb.velocity.x) > speed)
                {
                    mPlayerMotor.SetVelocityX(speed * Mathf.Sign(mPlayerMotor.Rb.velocity.x));
                }
            }
        }

        private void UpdateSprintStateOnLand()
        {
            SelectSpeed(mPlayerMotor.IsSprintDown);
        }

        // Set the speed of player.
        public void SelectSpeed(bool shouldSprint = false)
        {
            if (!mPlayerMotor.IsGrounded)
            {
                return;
            }

            if (!bHoldToSprint)
            {
                speed = runSpeed;
                return;
            }


            if (shouldSprint)
            {
                speed = runSpeed;
            }
            else
            {
                speed = walkSpeed;
            }
        }

        //private float IncrementTowards(float currentValue, float targetValue, float rate)
        //{
        //    if (currentValue == targetValue)
        //    {
        //        return currentValue;
        //    }
        //    else
        //    {
        //        float dir = Mathf.Sign(targetValue - currentValue);
        //        currentValue += rate * Time.deltaTime * dir;
        //        return (dir == Mathf.Sign(targetValue - currentValue)) ? currentValue : targetValue;
        //    }
        //}
    }
}