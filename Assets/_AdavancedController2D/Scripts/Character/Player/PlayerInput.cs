﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;
using HrtzzUtilities;
using Saya.InputSystem;

namespace Saya.Controller2D
{
    [RequireComponent(typeof(PlayerMotor))]
    public class PlayerInput : MonoBehaviour
    {
        public InputControlType dashButton;
        public InputControlType sprintButton;
        public InputControlType bashButton;

        private PlayerActions mPlayerActions;
        private PlayerMotor mPlayerMotor;

        public bool isInitiialised = true;

        private void OnEnable()
        {
            if (sprintButton == InputControlType.None || dashButton == InputControlType.None || bashButton == InputControlType.None)
            {
                HrtzzLogger.LogWarning("Forgot to assign some input. Check PlayerInput class in editor.");
            }

            mPlayerActions =
                PlayerActions.CreateWithDefaultBindings(new InputScheme(dashButton, sprintButton, bashButton));
        }

        private void Start()
        {
            PlayerPrefs.DeleteAll();
            mPlayerMotor = GetComponent<PlayerMotor>();
        }

        public void EnableInputs()
        {
            isInitiialised = true;
        }

        public void DisableInputs()
        {
            isInitiialised = false;
            mPlayerMotor.SetDirectionalInput(Vector2.zero);
        }

        private void Update()
        {
            if (!isInitiialised)
            {
                return;  // Do not proceed further if input is not active
            }

            //if (Input.GetButtonDown("Jump"))
            //{
            //    Debug.Log("Hello");
            //    mPlayerMotor.OnJumpPressed();
            //}

            //if (Input.GetButtonUp("Jump"))
            //{
            //    mPlayerMotor.OnJumpReleased();
            //}

            //mPlayerMotor.SetDirectionalInput(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical") * Time.deltaTime));

            if (mPlayerActions.LeftStickInput.HasChanged)
            {
                mPlayerMotor.SetDirectionalInput(mPlayerActions.LeftStickInput.Value);
            }

            if (mPlayerActions.Bash.WasPressed)
            {
                mPlayerMotor.OnBashButtonStateChanged(true);
            }

            if (mPlayerActions.Bash.WasReleased)
            {
                mPlayerMotor.OnBashButtonStateChanged(false);
            }

            if (mPlayerActions.RightStickInput.HasChanged)
            {
                mPlayerMotor.SetCameraInput(mPlayerActions.RightStickInput.Value);
            }

            if (mPlayerActions.Sprint.WasPressed)
            {
                mPlayerMotor.OnSprintStateChanged(true);
            }

            if (mPlayerActions.Sprint.WasReleased)
            {
                mPlayerMotor.OnSprintStateChanged(false);
            }

            if (mPlayerActions.Jump.WasPressed)
            {
                mPlayerMotor.OnJumpPressed();
            }

            if (mPlayerActions.Jump.WasReleased)
            {
                mPlayerMotor.OnJumpReleased();
            }

            if (mPlayerActions.Dash.WasPressed)
            {
                mPlayerMotor.OnDashButtonPressed();
            }

            if (mPlayerActions.BaseAttack.WasPressed)
            {
                mPlayerMotor.OnBaseAttackPressed();
            }

            mPlayerMotor.Tick();
        }

        private void FixedUpdate()
        {
            if (isInitiialised)
            {
                mPlayerMotor.FixedTick();
            }
        }

        private void OnDisable()
        {
            mPlayerActions.Destroy();
        }
    }
}