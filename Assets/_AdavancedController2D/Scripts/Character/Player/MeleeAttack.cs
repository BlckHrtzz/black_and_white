﻿using System;
using UnityEngine;
using Saya.WeaponSystem;
using Com.LuisPedroFonseca.ProCamera2D;
using Saya.DamageSystem;
using HrtzzUtilities;

namespace Saya.Controller2D
{
    public enum WeaponType
    {
        Sword,
        LongSword
    }

    [RequireComponent(typeof(PlayerMotor))]
    public class MeleeAttack : MonoBehaviour
    {
        [Tooltip("All the available weapons")] [SerializeField]
        private Weapon[] PlayerWeapons;

        public WeaponType selectedWeapon;
        public float attacksPerSecond = 1;

        [Range(0.05f, 0.15f)] public float attackCheckDuration = 0.2f; // Should be lower than attack cooldown duration.

        private float m_GapBetweenAttacks = 1;
        private float m_AttackTimer = 0;
        private bool m_IsAttacking = false;

        private Weapon m_PlayerWeapon;
        private PlayerMotor m_PlayerMotor;
        private Damager m_Damager;

        private void Awake()
        {
            m_PlayerMotor = GetComponent<PlayerMotor>();

            SwitchWeapon(selectedWeapon); // Instantiate Weapon at the beginning.

            m_GapBetweenAttacks = 1 / attacksPerSecond;
//            DisableAttack();
        }


        private void OnEnable()
        {
            if (m_PlayerMotor)
            {
                m_PlayerMotor.BaseAttackPressed += Attack;
            }
        }

        private void OnDisable()
        {
            if (m_PlayerMotor)
            {
                m_PlayerMotor.BaseAttackPressed -= Attack;
            }

            if (m_Damager)
            {
                m_Damager.OnDamageableHit.RemoveListener(OnDamageableHit);
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                selectedWeapon = WeaponType.LongSword;
                SwitchWeapon(selectedWeapon);
            }
            else if (Input.GetKeyDown(KeyCode.O))
            {
                selectedWeapon = WeaponType.Sword;
                SwitchWeapon(WeaponType.Sword);
            }

            if (m_IsAttacking)
            {
                if (m_AttackTimer <= m_GapBetweenAttacks)
                {
                    m_AttackTimer += Time.deltaTime;
                }
                else
                {
                    m_IsAttacking = false;
                    m_AttackTimer = 0;
                }
            }
        }

        /// <summary>
        /// Returns the selected weapon from the weapon array.
        /// </summary>
        /// <param name="selectedWeaponType"></param>
        /// <returns></returns>
        private Weapon GetWeaponFromWeaponArray(WeaponType selectedWeaponType)
        {
            if (PlayerWeapons.Length <= 0)
            {
                HrtzzLogger.LogWarning("Weapon array is null");
                return null;
            }

            foreach (var weapon in PlayerWeapons)
            {
                if (weapon.WeaponType == selectedWeaponType)
                {
                    return weapon;
                }
            }

            HrtzzLogger.LogWarning("Requested Weapon is not present in Array");
            return null;
        }

        public void SwitchWeapon(WeaponType newWeapon)
        {
            if (m_PlayerWeapon)
            {
                // TODO : Can be pooled if continuous weapon switching is required. 
                DestroyImmediate(m_PlayerWeapon.gameObject);
            }

            Transform weaponOrigin;
            m_PlayerWeapon = Instantiate(GetWeaponFromWeaponArray(newWeapon), (weaponOrigin = transform).position,
                weaponOrigin.rotation,
                weaponOrigin);

            if (m_PlayerWeapon)
            {
                m_Damager = m_PlayerWeapon.Damager;
                if (m_Damager)
                {
                    m_Damager.OnDamageableHit.AddListener(OnDamageableHit);
                    m_Damager.spriteRenderer = m_PlayerMotor.PlayerGraphic;
                }

                // Disable the weapon.
                DisableAttack();
            }
        }

        private void Attack()
        {
            if (m_IsAttacking)
            {
                return;
            }

            EnableAttack();
            m_IsAttacking = true;
            m_PlayerWeapon.transform.localEulerAngles = new Vector3(0, m_PlayerMotor.IsFacingRight ? 0 : 180, 0);
            m_PlayerWeapon.PlayeWeaponEffect();
            Invoke(nameof(DisableAttack), attackCheckDuration);
        }

        private void EnableAttack()
        {
            m_Damager.EnableDamage();
            m_Damager.gameObject.SetActive(true);
        }

        private void DisableAttack()
        {
            m_Damager.gameObject.SetActive(false);
            m_Damager.DisableDamage();
        }

        private void OnDamageableHit(Damager damager, Damageable damageable)
        {
            //ProCamera2DShake.Instance.ShakeUsingPreset("EnemyHit");
        }
    }
}