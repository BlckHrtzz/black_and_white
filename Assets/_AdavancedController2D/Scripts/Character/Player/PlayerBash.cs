﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HrtzzUtilities;
using Saya.Interactables;

namespace Saya.Controller2D
{
    [RequireComponent(typeof(PlayerMotor))]
    public class PlayerBash : MonoBehaviour
    {
        [SerializeField] [Range(1, 5)] private float bashRadius = 2f;

        [SerializeField] private float m_BashDistance = 5f;
        [SerializeField] private float m_BashDuration = 0.25f;

        private float m_BashTimer = 0;

        private float m_BashForce = 5f;
        private bool m_IsCooldown = false;
        private bool m_IsBashing = false;
        private bool m_IsBashButtonDown = false;

        private GameObject m_Arrow;
        private Basheable m_Basheable;

        [SerializeField] private LayerMask bashLayer = Layers.Default;

        private Vector2 m_BashDirection = Vector2.up;

        private PlayerMotor m_PlayerMotor;

        private void Awake()
        {
            m_PlayerMotor = GetComponent<PlayerMotor>();
            m_BashForce = Mathf.Sqrt((2 * m_BashDistance) * -Physics2D.gravity.y);
        }

        private void OnEnable()
        {
            if (m_PlayerMotor)
            {
                m_PlayerMotor.BashButtonStateChangedEvent += SetBashButtonState;
            }
        }

        private void OnDisable()
        {
            if (m_PlayerMotor)
            {
                m_PlayerMotor.BashButtonStateChangedEvent += SetBashButtonState;
            }
        }

        private void Start()
        {
            m_IsBashing = false;
            m_IsBashButtonDown = false;
            m_Arrow = Instantiate(Resources.Load<GameObject>("Prefabs/Arrow"));
            if (!m_Arrow)
            {
                Debug.Log("Unable To Arrow Prefab");
            }
            else
            {
                m_Arrow.SetActive(false);
            }
        }

        // Update is called once per frame
        private void Update()
        {
            if (m_IsCooldown)
            {
                m_BashTimer += Time.deltaTime;

                if (m_BashTimer >= m_BashDuration)
                {
                    m_IsCooldown = false;
                    m_BashTimer = 0;
                    m_IsBashing = false;
                    m_PlayerMotor.Bash(false);
                }

                return;
            }


            if (m_IsBashButtonDown)
            {
                SearchBashTarget();
            }
            else if (!m_IsBashButtonDown && m_IsBashing)
            {
                Bash();
            }
        }


        private void SetBashButtonState(bool state)
        {
            m_IsBashButtonDown = state;
        }

        private void SearchBashTarget()
        {
            if (!m_IsBashing)
            {
                Collider2D collider = Physics2D.OverlapCircle(transform.position, bashRadius, bashLayer);

                if (collider)
                {
                    m_Basheable = collider.GetComponent<Basheable>();
                    if (m_Basheable)
                    {
                        m_Arrow.transform.position = m_Basheable.transform.position;
                        m_IsBashing = true;
                        m_Arrow.SetActive(true);
                        Time.timeScale = 0;
                        m_PlayerMotor.Bash(true);
                    }
                }
            }
            else
            {
                CalculateBashDirection();
            }
        }

        private void CalculateBashDirection()
        {
            if (m_PlayerMotor.RightStickInput != Vector2.zero)
            {
                m_BashDirection = m_PlayerMotor.RightStickInput;
            }

            m_BashDirection.Normalize();
            //bashDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition) - bashObject.transform.position;

            float rotationZ = Mathf.Atan2(m_BashDirection.y, m_BashDirection.x) * Mathf.Rad2Deg;
            m_Arrow.transform.rotation = Quaternion.Euler(0, 0, rotationZ);
        }

        private void ShiftPositions()
        {
            float width = m_Basheable.Sr.bounds.size.x;
            float height = m_Basheable.Sr.bounds.size.y;

            float tanFactor = Mathf.Atan2(m_BashDirection.y, m_BashDirection.x);

            int roundCos = Mathf.RoundToInt(Mathf.Cos(tanFactor));
            int roundSin = Mathf.RoundToInt(Mathf.Sin(tanFactor));
            var bashObjectPos = m_Basheable.transform.position;
            m_PlayerMotor.Rb.position = new Vector2(bashObjectPos.x + (width * roundCos), bashObjectPos.y + (height * roundSin));
        }

        private void Bash()
        {
            //ShiftPositions();
            m_PlayerMotor.Rb.gravityScale = 1;
            m_PlayerMotor.Rb.velocity = Vector2.zero;
            m_Basheable.Rb.velocity = Vector2.zero;

            m_PlayerMotor.Rb.AddForce(m_BashDirection * m_BashForce, ForceMode2D.Impulse);
            m_Basheable.Bash(-m_BashDirection * m_BashForce);

            m_IsCooldown = true;
            m_IsBashing = false;
            m_Basheable = null;
            m_Arrow.SetActive(false);
            Time.timeScale = 1;
        }
    }
}