﻿using System;
using UnityEngine;
using HrtzzUtilities;

namespace Saya.Controller2D
{
    [RequireComponent(typeof(PlayerMotor))]
    public class PlayerDash : MonoBehaviour
    {
        [Header("DashSettings")] [Tooltip("The distance covered by dash.")] [Range(2f, 8f)] [SerializeField]
        private float dashDistance = 4.0f; // Distance covered by dash.

        private Vector2 dashStartPos = Vector2.zero;
        private Vector2 dashEndPos = Vector2.zero;

        [Tooltip("Time required to cover the dash distance.")] [Range(0.2f, 2f)] [SerializeField]
        private float dashDuration = 0.5f; // Time to cover the dash distance;

        private float dashStartTime = 0f; // Updates whenever the dash is started.

        [Tooltip("How frequently the player is allowed to dash.")] [Range(0.25f, 2f)] [SerializeField]
        private float dashCooldownDuration = 1f;

        private float dashCoolDownTimer = 0f;
        private bool isDashCoolingDown = false;

        [SerializeField] private bool canAirDash = true;
        [SerializeField] private bool isAirDashLimited = true;
        [SerializeField] private int maxAirDashes = 1;
        [SerializeField] private LayerMask dashCollisionLayers = Layers.Default;

        private int airDashCount = 0;

        private bool isAllowedToDash = false;
        private bool dash = false;

        private PlayerMotor playerMotor;

        // Lerp Variables.

        #region Unity Callbacks

        private void Awake()
        {
            playerMotor = GetComponent<PlayerMotor>();
        }

        private void OnEnable()
        {
            if (playerMotor)
            {
                playerMotor.PlayerLandedEvent += ResetAirDashes;
                playerMotor.DashButtonPressedEvent += InitiateDash;
                playerMotor.OnFixedUpdateEvent += ManagedFixedUpdate;
            }
        }

        private void OnDisable()
        {
            if (playerMotor)
            {
                playerMotor.PlayerLandedEvent -= ResetAirDashes;
                playerMotor.DashButtonPressedEvent -= InitiateDash;
                playerMotor.OnFixedUpdateEvent -= ManagedFixedUpdate;
            }
        }

        private void Update()
        {
            if (isDashCoolingDown)
            {
                if (dashCoolDownTimer <= dashCooldownDuration)
                {
                    dashCoolDownTimer += Time.deltaTime;
                }
                else
                {
                    dashCoolDownTimer = 0;
                    isDashCoolingDown = false;
                }
            }
        }

        private void ManagedFixedUpdate()
        {
            if (dash)
            {
                playerMotor.Rb.MovePosition(BetterLerp(dashStartPos, dashEndPos, dashStartTime, dashDuration, OnDashEnd));
                //  Stop the dash if Any collision is detected.
                if (Physics2D.OverlapArea(playerMotor.FrontCheckTop.position, playerMotor.FrontCheckBottom.position, dashCollisionLayers))
                {
                    OnDashEnd();
                }
            }
        }

        #endregion

        #region UserDefined Functions.

        // Initiates the dash if the required conditions are met.
        private void InitiateDash()
        {
            SetDashAllowed();
            if (!isDashCoolingDown && !dash && isAllowedToDash)
            {
                playerMotor.Dash(true);
                dash = true;
                dashStartPos = transform.position;
                dashEndPos = dashStartPos + Vector2.right * dashDistance * (playerMotor.IsFacingRight ? 1 : -1);
                dashStartTime = Time.time;
                if (!playerMotor.IsGrounded)
                {
                    airDashCount++;
                }
            }
        }

        // Called when the dash ends.
        private void OnDashEnd()
        {
            isDashCoolingDown = true;
            dash = false;
            playerMotor.Dash(dash);
        }

        private void SetDashAllowed()
        {
            if (!playerMotor.IsGrounded)
            {
                // Dashing is not allowed if the player is in air and Air dash is set to false.
                if (!canAirDash)
                {
                    isAllowedToDash = false;
                }
                // Allow player player to airDashCount is less then max air dashes or there is no air dash limit.
                else if ((isAirDashLimited && airDashCount < maxAirDashes) || !isAirDashLimited)
                {
                    isAllowedToDash = true;
                }
                else
                {
                    isAllowedToDash = false;
                }
            }
            // Allow player to dash when grounded.
            else
            {
                isAllowedToDash = true;
            }
        }

        private void ResetAirDashes()
        {
            airDashCount = 0;
        }

        private Vector2 BetterLerp(Vector2 startVector, Vector2 finalVector, float lerpStartTime, float lerpDuration = 1, Action onLerpCompleted = null)
        {
            float lerpElapsedTime = Time.time - lerpStartTime;
            // Clamp th elapsed time if it's greater then lerp time.
            if (lerpElapsedTime >= lerpDuration)
            {
                lerpElapsedTime = lerpDuration;
                onLerpCompleted?.Invoke();
            }

            float percentComplete = lerpElapsedTime / lerpDuration;
            //percentComplete = percentComplete * percentComplete * (3f - 2f * percentComplete);
            percentComplete = Mathf.Sin(percentComplete * Mathf.PI * 0.5f);
            return Vector2.Lerp(startVector, finalVector, percentComplete);
        }

        #endregion
    }
}