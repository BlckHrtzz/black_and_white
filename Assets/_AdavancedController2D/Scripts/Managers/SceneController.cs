﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HrtzzUtilities;
using Saya.SaveSystem;
using Unity.Collections;
using UnityEngine.SceneManagement;

namespace Saya
{
    public class SceneController : HrtzzAutoSingleton<SceneController>
    {
        public int sceneIndex;
        public int goalCount;
        public int currentCount;

        private Dictionary<int, CheckPoint> m_SceneCheckPoints = new Dictionary<int, CheckPoint>();

        [SerializeField] [Tooltip("Assign the default checkpoint for scene")]
        private CheckPoint m_CurrentCheckPoint;

        public CheckPoint CurrentCheckPoint => m_CurrentCheckPoint;

        // Start is called before the first frame update
        protected override void Awake()
        {
            base.Awake();
            SetCheckPoints();
        }


        private void SetCheckPoints()
        {
            CheckPoint[] checkPoints = FindObjectsOfType<CheckPoint>();
            foreach (var cp in checkPoints)
            {
                if (!m_SceneCheckPoints.ContainsKey(cp.GetInstanceID()))
                {
                    m_SceneCheckPoints.Add(cp.GetInstanceID(), cp);
                }
            }

            if (PlayerPrefs.HasKey("ActiveCheckPoint"))
            {
                int currentCheckPointId = PlayerPrefs.GetInt("ActiveCheckPoint");
                if (m_SceneCheckPoints.ContainsKey(currentCheckPointId))
                {
                    m_SceneCheckPoints[currentCheckPointId].SetSelfAsActiveCheckpoint(0);
                    HrtzzLogger.Log("Saved CheckPoint found");
                }
            }
            else if (m_CurrentCheckPoint)
            {
                HrtzzLogger.Log("Loading default Check Point");
                m_CurrentCheckPoint.SetSelfAsActiveCheckpoint(0);
            }
            else
            {
                HrtzzLogger.Log("Please Assign default CheckPoint");
            }
        }

        public void UpdateCurrentCheckpoint(CheckPoint checkPoint)
        {
            // For preventing first time load bug.
            if (checkPoint != m_CurrentCheckPoint)
            {
                if (m_CurrentCheckPoint)
                {
                    m_CurrentCheckPoint.DeactivateSelf();
                }

                m_CurrentCheckPoint = checkPoint;
                PlayerPrefs.SetInt("ActiveCheckPoint", checkPoint.InstanceId);
            }
        }

        public void LoadScene()
        {
            currentCount++;

            if (goalCount <= currentCount)
            {
                SceneManager.LoadScene(sceneIndex);
            }
        }
    }
}