﻿using Saya.Controller2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIInputController : MonoBehaviour
{
    public Button leftButton;
    public Button rightButton;
    public Button JumpButton;
    public PlayerMotor mPlayerMotor;

    private void Start()
    {
    }

    void Move(float value)
    {
        mPlayerMotor.SetDirectionalInput(new Vector2(value, 0));
    }

    void JumpPressed()
    {
        mPlayerMotor.OnJumpPressed();
    }

    void JumpReleased()
    {
        mPlayerMotor.OnJumpReleased();
    }


    
}
