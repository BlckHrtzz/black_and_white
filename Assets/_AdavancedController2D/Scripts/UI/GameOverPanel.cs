﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverPanel : MonoBehaviour
{
    private CanvasGroup group;

    private void Start()
    {
        group = GetComponent<CanvasGroup>();
        HideGameOverPanel();
    }

    public void HideGameOverPanel()
    {
        group.alpha = 1;
        gameObject.SetActive(true);
        group.DOFade(0, 1);
    }
    public void ShowGameOverPanel()
    {
        group.alpha = 0;
        gameObject.SetActive(true);
        group.DOFade(1, 1);
    }
}
