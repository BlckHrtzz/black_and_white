﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DialoguePopup : MonoBehaviour, ITriggerable
{
    public float popupDuration = 5f;
    
    public void OnTrigerExit()
    {
    }

    public void OnTriggerEnter()
    {
        gameObject.SetActive(true);
        DOVirtual.DelayedCall(popupDuration, DisableSelf);

    }

    void DisableSelf()
    {
        gameObject.SetActive(false);
    }
}
