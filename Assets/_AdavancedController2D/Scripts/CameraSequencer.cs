﻿using Com.LuisPedroFonseca.ProCamera2D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CameraSequencer : MonoBehaviour
{
    public ProCamera2D cam;
    public Transform tempTarget;
    public Transform player;
    public Saya.Controller2D.PlayerInput playerInput;


    public void ShiftTargetToNPC()
    {
        cam.RemoveAllCameraTargets();
        cam.AddCameraTarget(tempTarget);
        playerInput.DisableInputs();
        playerInput.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }

    public void ShiftTargetToPlayer()
    {
        cam.RemoveAllCameraTargets();
        cam.AddCameraTarget(player);
        playerInput.EnableInputs();
        playerInput.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }


}
