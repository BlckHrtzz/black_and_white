﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using InControl;

namespace Saya.InputSystem
{
    public class PlayerActions : PlayerActionSet
    {
        public PlayerAction Jump;
        public PlayerAction Sprint;
        public PlayerAction Dash;
        public PlayerAction Bash;
        public PlayerAction BaseAttack;
        public PlayerAction Left, Right, Up, Down;
        public PlayerTwoAxisAction LeftStickInput;
        public PlayerAction CameraLeft, CameraRight, CameraUp, CameraDown;
        public PlayerTwoAxisAction RightStickInput;

        public PlayerActions()
        {
            Jump = CreatePlayerAction("Jump");
            Sprint = CreatePlayerAction("Sprint");
            Dash = CreatePlayerAction("Dash");
            Bash = CreatePlayerAction("Bash");
            BaseAttack = CreatePlayerAction("BaseAttack");


            Left = CreatePlayerAction("MoveLeft");
            Right = CreatePlayerAction("MoveRight");
            Down = CreatePlayerAction("MoveDown");
            Up = CreatePlayerAction("MoveUp");
            LeftStickInput = CreateTwoAxisPlayerAction(Left, Right, Down, Up);

            CameraUp = CreatePlayerAction("CameraUp");
            CameraDown = CreatePlayerAction("CameraDown");
            CameraLeft = CreatePlayerAction("CameraLeft");
            CameraRight = CreatePlayerAction("CameraRight");
            RightStickInput = CreateTwoAxisPlayerAction(CameraLeft, CameraRight, CameraDown, CameraUp);
        }

        public static PlayerActions CreateWithDefaultBindings(InputScheme customScheme)
        {
            var playerActions = new PlayerActions();

            playerActions.Jump.AddDefaultBinding(Key.Space);
            playerActions.Jump.AddDefaultBinding(InputControlType.Action1);

            playerActions.Sprint.AddDefaultBinding(Key.LeftShift);
            playerActions.Sprint.AddDefaultBinding(customScheme.SprintButton);

            playerActions.Dash.AddDefaultBinding(Key.LeftAlt);
            playerActions.Dash.AddDefaultBinding(customScheme.DashButton);

            // TODO : Keyboard binding needs to be added.
            //playerActions.Bash.AddDefaultBinding(Key.LeftControl);
            playerActions.Bash.AddDefaultBinding(customScheme.BashButton);

            playerActions.BaseAttack.AddDefaultBinding(Mouse.LeftButton);
            playerActions.BaseAttack.AddDefaultBinding(InputControlType.Action3);

            // Keyboard WSAD Bindings.
            playerActions.Up.AddDefaultBinding(Key.W);
            playerActions.Down.AddDefaultBinding(Key.S);
            playerActions.Left.AddDefaultBinding(Key.A);
            playerActions.Right.AddDefaultBinding(Key.D);

            // Gamepad LeftStick Binding.
            playerActions.Up.AddDefaultBinding(InputControlType.LeftStickUp);
            playerActions.Down.AddDefaultBinding(InputControlType.LeftStickDown);
            playerActions.Left.AddDefaultBinding(InputControlType.LeftStickLeft);
            playerActions.Right.AddDefaultBinding(InputControlType.LeftStickRight);

            // Keyboard Arrow Bindings.
            playerActions.CameraUp.AddDefaultBinding(Key.UpArrow);
            playerActions.CameraDown.AddDefaultBinding(Key.DownArrow);
            playerActions.CameraLeft.AddDefaultBinding(Key.LeftArrow);
            playerActions.CameraRight.AddDefaultBinding(Key.RightArrow);
            // Gamepad RightStick Binding.
            playerActions.CameraUp.AddDefaultBinding(InputControlType.RightStickUp);
            playerActions.CameraDown.AddDefaultBinding(InputControlType.RightStickDown);
            playerActions.CameraLeft.AddDefaultBinding(InputControlType.RightStickLeft);
            playerActions.CameraRight.AddDefaultBinding(InputControlType.RightStickRight);

            /** Trash
            playerActions.ListenOptions.IncludeUnknownControllers = true;
            playerActions.ListenOptions.MaxAllowedBindings = 4;
            playerActions.ListenOptions.UnsetDuplicateBindingsOnSet = true;

            playerActions.ListenOptions.OnBindingFound = (action, binding) =>
            {
                if (binding == new KeyBindingSource(Key.Escape))
                {
                    action.StopListeningForBinding();
                    return false;
                }
                return true;
            };

            playerActions.ListenOptions.OnBindingAdded += (action, binding) =>
            {
                Debug.Log("Binding added... " + binding.DeviceName + ": " + binding.Name);
            };

            playerActions.ListenOptions.OnBindingRejected += (action, binding, reason) =>
            {
                Debug.Log("Binding rejected... " + reason);
            };
            */

            return playerActions;
        }
    }

    public struct InputScheme
    {
        public InputControlType DashButton { get; private set; }
        public InputControlType SprintButton { get; private set; }
        public InputControlType BashButton { get; private set; }

        public InputScheme(InputControlType dashButton, InputControlType sprintButton, InputControlType bashButton)
        {
            DashButton = dashButton;
            SprintButton = sprintButton;
            BashButton = bashButton;
        }
    }
}