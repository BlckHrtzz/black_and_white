﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
public class Switch : MonoBehaviour
{
    public SpeciesType speciesToAllow;
    public TriggerEvent onTrgiggerEnter;
    public TriggerEvent onTrgiggerExit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Species species = collision.gameObject.GetComponent<Species>();
        if (species && species.speciesType == speciesToAllow)
        {
            DisableSelf();
            onTrgiggerEnter.Invoke(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        onTrgiggerExit.Invoke(collision.gameObject);
    }

    void DisableSelf()
    {
        gameObject.SetActive(false);
    }
}

[System.Serializable]
public class TriggerEvent : UnityEvent<GameObject>
{

}
