﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Saya.Interactables
{
    [RequireComponent(typeof(Rigidbody2D), typeof(SpriteRenderer))]
    public abstract class Basheable : MonoBehaviour
    {
        public SpriteRenderer Sr { get; private set; }
        public Rigidbody2D Rb { get; private set; }

        // Start is called before the first frame update
        protected virtual void Awake()
        {
            Sr = GetComponent<SpriteRenderer>();
            Rb = GetComponent<Rigidbody2D>();
        }

        public virtual void Bash(Vector2 bashDirection)
        {
            Rb.AddForce(bashDirection, ForceMode2D.Impulse);
        }
    }
}