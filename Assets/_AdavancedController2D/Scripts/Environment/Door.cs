﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using HrtzzUtilities;
using DG.Tweening;
using UnityEngine.Events;

public class Door : MonoBehaviour, ITriggerable
{
    public UnityEvent doorOpened;

    public Vector2 openPos;
    public Vector2 closePos;
    bool isOpening = false;

    public float duration = 1f;

    public void OnTriggerEnter()
    {
        if (isOpening)
        {
            return;
        }

        isOpening = true;
        transform.DOMove(openPos, duration).OnComplete(delegate
        {
            doorOpened.Invoke();
        });
    }

    public void OnTrigerExit()
    {
    }


}
