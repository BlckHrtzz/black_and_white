﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Saya.Level
{
    public class GroundCollider : MonoBehaviour
    {
        SpriteRenderer sr;
        public SpriteRenderer SpriteRenderer
        {
            get
            {
                if (!sr)
                {
                    sr = GetComponent<SpriteRenderer>();
                }
                return sr;
            }
        }

        void Reset()
        {
            sr = GetComponent<SpriteRenderer>();
        }

        public void SetColliderColor(Color color)
        {
            SpriteRenderer.color = color;
        }

        public void ToggleSpriteState()
        {
            SpriteRenderer.enabled = !SpriteRenderer.enabled;
        }
    }
}
