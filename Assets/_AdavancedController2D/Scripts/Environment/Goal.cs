﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;


public class Goal : MonoBehaviour
{
    public UnityEvent OnGoalReached;
    public SpeciesType speciesToAllow = SpeciesType.Black;
    Species species;
    bool isGameEnd = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        species = collision.gameObject.GetComponent<Species>();
        if (species && species.speciesType == speciesToAllow)
        {
            DOVirtual.DelayedCall(1, GoalReached);
            if (!species.GetComponent<Saya.Controller2D.PlayerInput>())
            {
                species.ToggleSpeciesColor();
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Vector2.Distance(transform.position, species.transform.position) <= 0.5f && !isGameEnd)
        {
            isGameEnd = true;

            if (species && species.speciesType == speciesToAllow)
            {
                Saya.Controller2D.PlayerInput playerInput = species.GetComponent<Saya.Controller2D.PlayerInput>();
                if (playerInput)
                {
                    species.ToggleSpeciesColor();
                    playerInput.DisableInputs();
                    Rigidbody2D rb = playerInput.GetComponent<Rigidbody2D>();
                    rb.isKinematic = true;
                    rb.velocity = Vector2.zero;
                    species.transform.position = transform.position + Vector3.up * -0.5f;
                }
            }
        }
    }

    void GoalReached()
    {
        OnGoalReached.Invoke();
    }

}
