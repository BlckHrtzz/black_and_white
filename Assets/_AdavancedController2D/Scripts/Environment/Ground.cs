﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Saya.Level
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class Ground : MonoBehaviour
    {
        SpriteRenderer sr;
        public SpriteRenderer SpriteRenderer
        {
            get
            {
                if (!sr)
                {
                    sr = GetComponent<SpriteRenderer>();
                }
                return sr;
            }
        }

        void Reset()
        {
            sr = GetComponent<SpriteRenderer>();
        }
    }
}
