﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(BoxCollider2D))]
public class DissolvingPlatform : MonoBehaviour
{
    public SpeciesType speciesToAllow = SpeciesType.Black;

    private BoxCollider2D m_Collider;
    private SpriteRenderer m_SpriteRenderer;

    [Range(0.1f, 2f)]
    public float dissolveDelay = 0.5f;
    [Range(0, 10)]
    public float hiddenDuration = 2.0f;

    private float m_TransitDuration = 0.25f;
    private float m_Timer = 0;


    // Start is called before the first frame update
    private void Awake()
    {
        m_Collider = GetComponent<BoxCollider2D>();
        m_SpriteRenderer = GetComponent<SpriteRenderer>();
        SetPlatformColor();
    }

    bool isPlayerOnPlatform = false;

    private void Update()
    {
        if (isPlayerOnPlatform)
        {
            m_Timer += Time.deltaTime;
            if (m_Timer >= m_TransitDuration)
            {
                m_Timer = 0;
                isPlayerOnPlatform = false;
                DissolvePlatform();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Species species = collision.gameObject.GetComponent<Species>();
        if (species && species.speciesType != speciesToAllow)
        {
            isPlayerOnPlatform = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        Species species = collision.gameObject.GetComponent<Species>();
        if (species && species.speciesType != speciesToAllow)
        {
            m_Timer = 0;
            isPlayerOnPlatform = false;
        }
    }

    private void InitPlatform()
    {
        SetPlatformColor();
    }

    void ChangePlatformType()
    {
        switch (speciesToAllow)
        {
            case SpeciesType.Black:
                speciesToAllow = SpeciesType.White;
                break;
            case SpeciesType.White:
                speciesToAllow = SpeciesType.Black;
                break;
        }
        SetPlatformColor();
    }

    private void SetPlatformColor()
    {
        if (m_SpriteRenderer)
        {
            switch (speciesToAllow)
            {
                case SpeciesType.Black:
                    m_SpriteRenderer.color = Color.black;
                    break;
                case SpeciesType.White:
                    m_SpriteRenderer.color = Color.white;
                    break;
            }
        }
    }


    private void DissolvePlatform()
    {
        m_Collider.enabled = false;
        m_SpriteRenderer.DOFade(0, m_TransitDuration).OnComplete(delegate
        {
            DOVirtual.DelayedCall(hiddenDuration, ShowPlatform);
        });
    }

    private void ShowPlatform()
    {
        m_SpriteRenderer.DOFade(1, m_TransitDuration).OnComplete(delegate
        {
            m_Collider.enabled = true;
        });
    }
}
