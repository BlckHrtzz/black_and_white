﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;

public class AIController : MonoBehaviour
{
    public Transform waypointContainer;
    public Transform[] wayPoints;

    public ReachedEvent onReached;

    private int currentIndex = 1;
    public float speed = 2;

    private void Awake()
    {
        wayPoints = waypointContainer.GetComponentsInChildren<Transform>();
    }

    public void MoveToNextIndex()
    {
        if (currentIndex < wayPoints.Length)
        {
            if (wayPoints[currentIndex])
            {
                float distance = Vector2.Distance(transform.position, wayPoints[currentIndex].position);
                float duration = distance / speed;
                transform.DOMove(wayPoints[currentIndex].position, duration).SetEase(Ease.Linear).OnComplete(delegate
                {
                    OnPositionReached(currentIndex);
                });
                currentIndex++;
            }
        }
    }

    public void OnPositionReached(int index)
    {
        if (index == currentIndex)
        {
            MoveToNextIndex();
        }
    }

}

[System.Serializable]
public class ReachedEvent : UnityEvent<int>
{

}
