// This file is auto-generated. Modifications are not saved.

namespace HrtzzUtilities
{
    public static class Tags
    {
        /// <summary>
        /// Name of tag 'Untagged'.
        /// </summary>
        public const string Untagged = "Untagged";

        /// <summary>
        /// Name of tag 'Respawn'.
        /// </summary>
        public const string Respawn = "Respawn";

        /// <summary>
        /// Name of tag 'Finish'.
        /// </summary>
        public const string Finish = "Finish";

        /// <summary>
        /// Name of tag 'EditorOnly'.
        /// </summary>
        public const string EditorOnly = "EditorOnly";

        /// <summary>
        /// Name of tag 'MainCamera'.
        /// </summary>
        public const string MainCamera = "MainCamera";

        /// <summary>
        /// Name of tag 'Player'.
        /// </summary>
        public const string Player = "Player";

        /// <summary>
        /// Name of tag 'GameController'.
        /// </summary>
        public const string GameController = "GameController";
    }

    public static class SortingLayers
    {
        /// <summary>
        /// ID of sorting layer 'Default'.
        /// </summary>
        public const int Default = 0;

        /// <summary>
        /// ID of sorting layer 'Particle'.
        /// </summary>
        public const int Particle = -1053932837;
    }

    public static class Layers
    {
        /// <summary>
        /// Index of layer 'Default'.
        /// </summary>
        public const int Default = 0;

        /// <summary>
        /// Index of layer 'TransparentFX'.
        /// </summary>
        public const int TransparentFX = 1;

        /// <summary>
        /// Index of layer 'Ignore Raycast'.
        /// </summary>
        public const int Ignore_Raycast = 2;

        /// <summary>
        /// Index of layer 'Water'.
        /// </summary>
        public const int Water = 4;

        /// <summary>
        /// Index of layer 'UI'.
        /// </summary>
        public const int UI = 5;

        /// <summary>
        /// Index of layer 'GroundLayer'.
        /// </summary>
        public const int GroundLayer = 8;

        /// <summary>
        /// Index of layer 'WallLayer'.
        /// </summary>
        public const int WallLayer = 9;

        /// <summary>
        /// Index of layer 'FarLayer'.
        /// </summary>
        public const int FarLayer = 10;

        /// <summary>
        /// Index of layer 'Player'.
        /// </summary>
        public const int Player = 11;

        /// <summary>
        /// Index of layer 'Enemy'.
        /// </summary>
        public const int Enemy = 12;

        /// <summary>
        /// Bitmask of layer 'Default'.
        /// </summary>
        public const int DefaultMask = 1 << 0;

        /// <summary>
        /// Bitmask of layer 'TransparentFX'.
        /// </summary>
        public const int TransparentFXMask = 1 << 1;

        /// <summary>
        /// Bitmask of layer 'Ignore Raycast'.
        /// </summary>
        public const int Ignore_RaycastMask = 1 << 2;

        /// <summary>
        /// Bitmask of layer 'Water'.
        /// </summary>
        public const int WaterMask = 1 << 4;

        /// <summary>
        /// Bitmask of layer 'UI'.
        /// </summary>
        public const int UIMask = 1 << 5;

        /// <summary>
        /// Bitmask of layer 'GroundLayer'.
        /// </summary>
        public const int GroundLayerMask = 1 << 8;

        /// <summary>
        /// Bitmask of layer 'WallLayer'.
        /// </summary>
        public const int WallLayerMask = 1 << 9;

        /// <summary>
        /// Bitmask of layer 'FarLayer'.
        /// </summary>
        public const int FarLayerMask = 1 << 10;

        /// <summary>
        /// Bitmask of layer 'Player'.
        /// </summary>
        public const int PlayerMask = 1 << 11;

        /// <summary>
        /// Bitmask of layer 'Enemy'.
        /// </summary>
        public const int EnemyMask = 1 << 12;
    }

    public static class Scenes
    {
        /// <summary>
        /// ID of scene 'PrototypeScene'.
        /// </summary>
        public const int PrototypeScene = 0;
    }

    public static class Axes
    {
        /// <summary>
        /// Input axis 'joystick 1 analog 0'.
        /// </summary>
        public const string joystick_1_analog_0 = "joystick 1 analog 0";

        /// <summary>
        /// Input axis 'joystick 1 analog 1'.
        /// </summary>
        public const string joystick_1_analog_1 = "joystick 1 analog 1";

        /// <summary>
        /// Input axis 'joystick 1 analog 2'.
        /// </summary>
        public const string joystick_1_analog_2 = "joystick 1 analog 2";

        /// <summary>
        /// Input axis 'joystick 1 analog 3'.
        /// </summary>
        public const string joystick_1_analog_3 = "joystick 1 analog 3";

        /// <summary>
        /// Input axis 'joystick 1 analog 4'.
        /// </summary>
        public const string joystick_1_analog_4 = "joystick 1 analog 4";

        /// <summary>
        /// Input axis 'joystick 1 analog 5'.
        /// </summary>
        public const string joystick_1_analog_5 = "joystick 1 analog 5";

        /// <summary>
        /// Input axis 'joystick 1 analog 6'.
        /// </summary>
        public const string joystick_1_analog_6 = "joystick 1 analog 6";

        /// <summary>
        /// Input axis 'joystick 1 analog 7'.
        /// </summary>
        public const string joystick_1_analog_7 = "joystick 1 analog 7";

        /// <summary>
        /// Input axis 'joystick 1 analog 8'.
        /// </summary>
        public const string joystick_1_analog_8 = "joystick 1 analog 8";

        /// <summary>
        /// Input axis 'joystick 1 analog 9'.
        /// </summary>
        public const string joystick_1_analog_9 = "joystick 1 analog 9";

        /// <summary>
        /// Input axis 'joystick 1 analog 10'.
        /// </summary>
        public const string joystick_1_analog_10 = "joystick 1 analog 10";

        /// <summary>
        /// Input axis 'joystick 1 analog 11'.
        /// </summary>
        public const string joystick_1_analog_11 = "joystick 1 analog 11";

        /// <summary>
        /// Input axis 'joystick 1 analog 12'.
        /// </summary>
        public const string joystick_1_analog_12 = "joystick 1 analog 12";

        /// <summary>
        /// Input axis 'joystick 1 analog 13'.
        /// </summary>
        public const string joystick_1_analog_13 = "joystick 1 analog 13";

        /// <summary>
        /// Input axis 'joystick 1 analog 14'.
        /// </summary>
        public const string joystick_1_analog_14 = "joystick 1 analog 14";

        /// <summary>
        /// Input axis 'joystick 1 analog 15'.
        /// </summary>
        public const string joystick_1_analog_15 = "joystick 1 analog 15";

        /// <summary>
        /// Input axis 'joystick 1 analog 16'.
        /// </summary>
        public const string joystick_1_analog_16 = "joystick 1 analog 16";

        /// <summary>
        /// Input axis 'joystick 1 analog 17'.
        /// </summary>
        public const string joystick_1_analog_17 = "joystick 1 analog 17";

        /// <summary>
        /// Input axis 'joystick 1 analog 18'.
        /// </summary>
        public const string joystick_1_analog_18 = "joystick 1 analog 18";

        /// <summary>
        /// Input axis 'joystick 1 analog 19'.
        /// </summary>
        public const string joystick_1_analog_19 = "joystick 1 analog 19";

        /// <summary>
        /// Input axis 'joystick 2 analog 0'.
        /// </summary>
        public const string joystick_2_analog_0 = "joystick 2 analog 0";

        /// <summary>
        /// Input axis 'joystick 2 analog 1'.
        /// </summary>
        public const string joystick_2_analog_1 = "joystick 2 analog 1";

        /// <summary>
        /// Input axis 'joystick 2 analog 2'.
        /// </summary>
        public const string joystick_2_analog_2 = "joystick 2 analog 2";

        /// <summary>
        /// Input axis 'joystick 2 analog 3'.
        /// </summary>
        public const string joystick_2_analog_3 = "joystick 2 analog 3";

        /// <summary>
        /// Input axis 'joystick 2 analog 4'.
        /// </summary>
        public const string joystick_2_analog_4 = "joystick 2 analog 4";

        /// <summary>
        /// Input axis 'joystick 2 analog 5'.
        /// </summary>
        public const string joystick_2_analog_5 = "joystick 2 analog 5";

        /// <summary>
        /// Input axis 'joystick 2 analog 6'.
        /// </summary>
        public const string joystick_2_analog_6 = "joystick 2 analog 6";

        /// <summary>
        /// Input axis 'joystick 2 analog 7'.
        /// </summary>
        public const string joystick_2_analog_7 = "joystick 2 analog 7";

        /// <summary>
        /// Input axis 'joystick 2 analog 8'.
        /// </summary>
        public const string joystick_2_analog_8 = "joystick 2 analog 8";

        /// <summary>
        /// Input axis 'joystick 2 analog 9'.
        /// </summary>
        public const string joystick_2_analog_9 = "joystick 2 analog 9";

        /// <summary>
        /// Input axis 'joystick 2 analog 10'.
        /// </summary>
        public const string joystick_2_analog_10 = "joystick 2 analog 10";

        /// <summary>
        /// Input axis 'joystick 2 analog 11'.
        /// </summary>
        public const string joystick_2_analog_11 = "joystick 2 analog 11";

        /// <summary>
        /// Input axis 'joystick 2 analog 12'.
        /// </summary>
        public const string joystick_2_analog_12 = "joystick 2 analog 12";

        /// <summary>
        /// Input axis 'joystick 2 analog 13'.
        /// </summary>
        public const string joystick_2_analog_13 = "joystick 2 analog 13";

        /// <summary>
        /// Input axis 'joystick 2 analog 14'.
        /// </summary>
        public const string joystick_2_analog_14 = "joystick 2 analog 14";

        /// <summary>
        /// Input axis 'joystick 2 analog 15'.
        /// </summary>
        public const string joystick_2_analog_15 = "joystick 2 analog 15";

        /// <summary>
        /// Input axis 'joystick 2 analog 16'.
        /// </summary>
        public const string joystick_2_analog_16 = "joystick 2 analog 16";

        /// <summary>
        /// Input axis 'joystick 2 analog 17'.
        /// </summary>
        public const string joystick_2_analog_17 = "joystick 2 analog 17";

        /// <summary>
        /// Input axis 'joystick 2 analog 18'.
        /// </summary>
        public const string joystick_2_analog_18 = "joystick 2 analog 18";

        /// <summary>
        /// Input axis 'joystick 2 analog 19'.
        /// </summary>
        public const string joystick_2_analog_19 = "joystick 2 analog 19";

        /// <summary>
        /// Input axis 'joystick 3 analog 0'.
        /// </summary>
        public const string joystick_3_analog_0 = "joystick 3 analog 0";

        /// <summary>
        /// Input axis 'joystick 3 analog 1'.
        /// </summary>
        public const string joystick_3_analog_1 = "joystick 3 analog 1";

        /// <summary>
        /// Input axis 'joystick 3 analog 2'.
        /// </summary>
        public const string joystick_3_analog_2 = "joystick 3 analog 2";

        /// <summary>
        /// Input axis 'joystick 3 analog 3'.
        /// </summary>
        public const string joystick_3_analog_3 = "joystick 3 analog 3";

        /// <summary>
        /// Input axis 'joystick 3 analog 4'.
        /// </summary>
        public const string joystick_3_analog_4 = "joystick 3 analog 4";

        /// <summary>
        /// Input axis 'joystick 3 analog 5'.
        /// </summary>
        public const string joystick_3_analog_5 = "joystick 3 analog 5";

        /// <summary>
        /// Input axis 'joystick 3 analog 6'.
        /// </summary>
        public const string joystick_3_analog_6 = "joystick 3 analog 6";

        /// <summary>
        /// Input axis 'joystick 3 analog 7'.
        /// </summary>
        public const string joystick_3_analog_7 = "joystick 3 analog 7";

        /// <summary>
        /// Input axis 'joystick 3 analog 8'.
        /// </summary>
        public const string joystick_3_analog_8 = "joystick 3 analog 8";

        /// <summary>
        /// Input axis 'joystick 3 analog 9'.
        /// </summary>
        public const string joystick_3_analog_9 = "joystick 3 analog 9";

        /// <summary>
        /// Input axis 'joystick 3 analog 10'.
        /// </summary>
        public const string joystick_3_analog_10 = "joystick 3 analog 10";

        /// <summary>
        /// Input axis 'joystick 3 analog 11'.
        /// </summary>
        public const string joystick_3_analog_11 = "joystick 3 analog 11";

        /// <summary>
        /// Input axis 'joystick 3 analog 12'.
        /// </summary>
        public const string joystick_3_analog_12 = "joystick 3 analog 12";

        /// <summary>
        /// Input axis 'joystick 3 analog 13'.
        /// </summary>
        public const string joystick_3_analog_13 = "joystick 3 analog 13";

        /// <summary>
        /// Input axis 'joystick 3 analog 14'.
        /// </summary>
        public const string joystick_3_analog_14 = "joystick 3 analog 14";

        /// <summary>
        /// Input axis 'joystick 3 analog 15'.
        /// </summary>
        public const string joystick_3_analog_15 = "joystick 3 analog 15";

        /// <summary>
        /// Input axis 'joystick 3 analog 16'.
        /// </summary>
        public const string joystick_3_analog_16 = "joystick 3 analog 16";

        /// <summary>
        /// Input axis 'joystick 3 analog 17'.
        /// </summary>
        public const string joystick_3_analog_17 = "joystick 3 analog 17";

        /// <summary>
        /// Input axis 'joystick 3 analog 18'.
        /// </summary>
        public const string joystick_3_analog_18 = "joystick 3 analog 18";

        /// <summary>
        /// Input axis 'joystick 3 analog 19'.
        /// </summary>
        public const string joystick_3_analog_19 = "joystick 3 analog 19";

        /// <summary>
        /// Input axis 'joystick 4 analog 0'.
        /// </summary>
        public const string joystick_4_analog_0 = "joystick 4 analog 0";

        /// <summary>
        /// Input axis 'joystick 4 analog 1'.
        /// </summary>
        public const string joystick_4_analog_1 = "joystick 4 analog 1";

        /// <summary>
        /// Input axis 'joystick 4 analog 2'.
        /// </summary>
        public const string joystick_4_analog_2 = "joystick 4 analog 2";

        /// <summary>
        /// Input axis 'joystick 4 analog 3'.
        /// </summary>
        public const string joystick_4_analog_3 = "joystick 4 analog 3";

        /// <summary>
        /// Input axis 'joystick 4 analog 4'.
        /// </summary>
        public const string joystick_4_analog_4 = "joystick 4 analog 4";

        /// <summary>
        /// Input axis 'joystick 4 analog 5'.
        /// </summary>
        public const string joystick_4_analog_5 = "joystick 4 analog 5";

        /// <summary>
        /// Input axis 'joystick 4 analog 6'.
        /// </summary>
        public const string joystick_4_analog_6 = "joystick 4 analog 6";

        /// <summary>
        /// Input axis 'joystick 4 analog 7'.
        /// </summary>
        public const string joystick_4_analog_7 = "joystick 4 analog 7";

        /// <summary>
        /// Input axis 'joystick 4 analog 8'.
        /// </summary>
        public const string joystick_4_analog_8 = "joystick 4 analog 8";

        /// <summary>
        /// Input axis 'joystick 4 analog 9'.
        /// </summary>
        public const string joystick_4_analog_9 = "joystick 4 analog 9";

        /// <summary>
        /// Input axis 'joystick 4 analog 10'.
        /// </summary>
        public const string joystick_4_analog_10 = "joystick 4 analog 10";

        /// <summary>
        /// Input axis 'joystick 4 analog 11'.
        /// </summary>
        public const string joystick_4_analog_11 = "joystick 4 analog 11";

        /// <summary>
        /// Input axis 'joystick 4 analog 12'.
        /// </summary>
        public const string joystick_4_analog_12 = "joystick 4 analog 12";

        /// <summary>
        /// Input axis 'joystick 4 analog 13'.
        /// </summary>
        public const string joystick_4_analog_13 = "joystick 4 analog 13";

        /// <summary>
        /// Input axis 'joystick 4 analog 14'.
        /// </summary>
        public const string joystick_4_analog_14 = "joystick 4 analog 14";

        /// <summary>
        /// Input axis 'joystick 4 analog 15'.
        /// </summary>
        public const string joystick_4_analog_15 = "joystick 4 analog 15";

        /// <summary>
        /// Input axis 'joystick 4 analog 16'.
        /// </summary>
        public const string joystick_4_analog_16 = "joystick 4 analog 16";

        /// <summary>
        /// Input axis 'joystick 4 analog 17'.
        /// </summary>
        public const string joystick_4_analog_17 = "joystick 4 analog 17";

        /// <summary>
        /// Input axis 'joystick 4 analog 18'.
        /// </summary>
        public const string joystick_4_analog_18 = "joystick 4 analog 18";

        /// <summary>
        /// Input axis 'joystick 4 analog 19'.
        /// </summary>
        public const string joystick_4_analog_19 = "joystick 4 analog 19";

        /// <summary>
        /// Input axis 'joystick 5 analog 0'.
        /// </summary>
        public const string joystick_5_analog_0 = "joystick 5 analog 0";

        /// <summary>
        /// Input axis 'joystick 5 analog 1'.
        /// </summary>
        public const string joystick_5_analog_1 = "joystick 5 analog 1";

        /// <summary>
        /// Input axis 'joystick 5 analog 2'.
        /// </summary>
        public const string joystick_5_analog_2 = "joystick 5 analog 2";

        /// <summary>
        /// Input axis 'joystick 5 analog 3'.
        /// </summary>
        public const string joystick_5_analog_3 = "joystick 5 analog 3";

        /// <summary>
        /// Input axis 'joystick 5 analog 4'.
        /// </summary>
        public const string joystick_5_analog_4 = "joystick 5 analog 4";

        /// <summary>
        /// Input axis 'joystick 5 analog 5'.
        /// </summary>
        public const string joystick_5_analog_5 = "joystick 5 analog 5";

        /// <summary>
        /// Input axis 'joystick 5 analog 6'.
        /// </summary>
        public const string joystick_5_analog_6 = "joystick 5 analog 6";

        /// <summary>
        /// Input axis 'joystick 5 analog 7'.
        /// </summary>
        public const string joystick_5_analog_7 = "joystick 5 analog 7";

        /// <summary>
        /// Input axis 'joystick 5 analog 8'.
        /// </summary>
        public const string joystick_5_analog_8 = "joystick 5 analog 8";

        /// <summary>
        /// Input axis 'joystick 5 analog 9'.
        /// </summary>
        public const string joystick_5_analog_9 = "joystick 5 analog 9";

        /// <summary>
        /// Input axis 'joystick 5 analog 10'.
        /// </summary>
        public const string joystick_5_analog_10 = "joystick 5 analog 10";

        /// <summary>
        /// Input axis 'joystick 5 analog 11'.
        /// </summary>
        public const string joystick_5_analog_11 = "joystick 5 analog 11";

        /// <summary>
        /// Input axis 'joystick 5 analog 12'.
        /// </summary>
        public const string joystick_5_analog_12 = "joystick 5 analog 12";

        /// <summary>
        /// Input axis 'joystick 5 analog 13'.
        /// </summary>
        public const string joystick_5_analog_13 = "joystick 5 analog 13";

        /// <summary>
        /// Input axis 'joystick 5 analog 14'.
        /// </summary>
        public const string joystick_5_analog_14 = "joystick 5 analog 14";

        /// <summary>
        /// Input axis 'joystick 5 analog 15'.
        /// </summary>
        public const string joystick_5_analog_15 = "joystick 5 analog 15";

        /// <summary>
        /// Input axis 'joystick 5 analog 16'.
        /// </summary>
        public const string joystick_5_analog_16 = "joystick 5 analog 16";

        /// <summary>
        /// Input axis 'joystick 5 analog 17'.
        /// </summary>
        public const string joystick_5_analog_17 = "joystick 5 analog 17";

        /// <summary>
        /// Input axis 'joystick 5 analog 18'.
        /// </summary>
        public const string joystick_5_analog_18 = "joystick 5 analog 18";

        /// <summary>
        /// Input axis 'joystick 5 analog 19'.
        /// </summary>
        public const string joystick_5_analog_19 = "joystick 5 analog 19";

        /// <summary>
        /// Input axis 'joystick 6 analog 0'.
        /// </summary>
        public const string joystick_6_analog_0 = "joystick 6 analog 0";

        /// <summary>
        /// Input axis 'joystick 6 analog 1'.
        /// </summary>
        public const string joystick_6_analog_1 = "joystick 6 analog 1";

        /// <summary>
        /// Input axis 'joystick 6 analog 2'.
        /// </summary>
        public const string joystick_6_analog_2 = "joystick 6 analog 2";

        /// <summary>
        /// Input axis 'joystick 6 analog 3'.
        /// </summary>
        public const string joystick_6_analog_3 = "joystick 6 analog 3";

        /// <summary>
        /// Input axis 'joystick 6 analog 4'.
        /// </summary>
        public const string joystick_6_analog_4 = "joystick 6 analog 4";

        /// <summary>
        /// Input axis 'joystick 6 analog 5'.
        /// </summary>
        public const string joystick_6_analog_5 = "joystick 6 analog 5";

        /// <summary>
        /// Input axis 'joystick 6 analog 6'.
        /// </summary>
        public const string joystick_6_analog_6 = "joystick 6 analog 6";

        /// <summary>
        /// Input axis 'joystick 6 analog 7'.
        /// </summary>
        public const string joystick_6_analog_7 = "joystick 6 analog 7";

        /// <summary>
        /// Input axis 'joystick 6 analog 8'.
        /// </summary>
        public const string joystick_6_analog_8 = "joystick 6 analog 8";

        /// <summary>
        /// Input axis 'joystick 6 analog 9'.
        /// </summary>
        public const string joystick_6_analog_9 = "joystick 6 analog 9";

        /// <summary>
        /// Input axis 'joystick 6 analog 10'.
        /// </summary>
        public const string joystick_6_analog_10 = "joystick 6 analog 10";

        /// <summary>
        /// Input axis 'joystick 6 analog 11'.
        /// </summary>
        public const string joystick_6_analog_11 = "joystick 6 analog 11";

        /// <summary>
        /// Input axis 'joystick 6 analog 12'.
        /// </summary>
        public const string joystick_6_analog_12 = "joystick 6 analog 12";

        /// <summary>
        /// Input axis 'joystick 6 analog 13'.
        /// </summary>
        public const string joystick_6_analog_13 = "joystick 6 analog 13";

        /// <summary>
        /// Input axis 'joystick 6 analog 14'.
        /// </summary>
        public const string joystick_6_analog_14 = "joystick 6 analog 14";

        /// <summary>
        /// Input axis 'joystick 6 analog 15'.
        /// </summary>
        public const string joystick_6_analog_15 = "joystick 6 analog 15";

        /// <summary>
        /// Input axis 'joystick 6 analog 16'.
        /// </summary>
        public const string joystick_6_analog_16 = "joystick 6 analog 16";

        /// <summary>
        /// Input axis 'joystick 6 analog 17'.
        /// </summary>
        public const string joystick_6_analog_17 = "joystick 6 analog 17";

        /// <summary>
        /// Input axis 'joystick 6 analog 18'.
        /// </summary>
        public const string joystick_6_analog_18 = "joystick 6 analog 18";

        /// <summary>
        /// Input axis 'joystick 6 analog 19'.
        /// </summary>
        public const string joystick_6_analog_19 = "joystick 6 analog 19";

        /// <summary>
        /// Input axis 'joystick 7 analog 0'.
        /// </summary>
        public const string joystick_7_analog_0 = "joystick 7 analog 0";

        /// <summary>
        /// Input axis 'joystick 7 analog 1'.
        /// </summary>
        public const string joystick_7_analog_1 = "joystick 7 analog 1";

        /// <summary>
        /// Input axis 'joystick 7 analog 2'.
        /// </summary>
        public const string joystick_7_analog_2 = "joystick 7 analog 2";

        /// <summary>
        /// Input axis 'joystick 7 analog 3'.
        /// </summary>
        public const string joystick_7_analog_3 = "joystick 7 analog 3";

        /// <summary>
        /// Input axis 'joystick 7 analog 4'.
        /// </summary>
        public const string joystick_7_analog_4 = "joystick 7 analog 4";

        /// <summary>
        /// Input axis 'joystick 7 analog 5'.
        /// </summary>
        public const string joystick_7_analog_5 = "joystick 7 analog 5";

        /// <summary>
        /// Input axis 'joystick 7 analog 6'.
        /// </summary>
        public const string joystick_7_analog_6 = "joystick 7 analog 6";

        /// <summary>
        /// Input axis 'joystick 7 analog 7'.
        /// </summary>
        public const string joystick_7_analog_7 = "joystick 7 analog 7";

        /// <summary>
        /// Input axis 'joystick 7 analog 8'.
        /// </summary>
        public const string joystick_7_analog_8 = "joystick 7 analog 8";

        /// <summary>
        /// Input axis 'joystick 7 analog 9'.
        /// </summary>
        public const string joystick_7_analog_9 = "joystick 7 analog 9";

        /// <summary>
        /// Input axis 'joystick 7 analog 10'.
        /// </summary>
        public const string joystick_7_analog_10 = "joystick 7 analog 10";

        /// <summary>
        /// Input axis 'joystick 7 analog 11'.
        /// </summary>
        public const string joystick_7_analog_11 = "joystick 7 analog 11";

        /// <summary>
        /// Input axis 'joystick 7 analog 12'.
        /// </summary>
        public const string joystick_7_analog_12 = "joystick 7 analog 12";

        /// <summary>
        /// Input axis 'joystick 7 analog 13'.
        /// </summary>
        public const string joystick_7_analog_13 = "joystick 7 analog 13";

        /// <summary>
        /// Input axis 'joystick 7 analog 14'.
        /// </summary>
        public const string joystick_7_analog_14 = "joystick 7 analog 14";

        /// <summary>
        /// Input axis 'joystick 7 analog 15'.
        /// </summary>
        public const string joystick_7_analog_15 = "joystick 7 analog 15";

        /// <summary>
        /// Input axis 'joystick 7 analog 16'.
        /// </summary>
        public const string joystick_7_analog_16 = "joystick 7 analog 16";

        /// <summary>
        /// Input axis 'joystick 7 analog 17'.
        /// </summary>
        public const string joystick_7_analog_17 = "joystick 7 analog 17";

        /// <summary>
        /// Input axis 'joystick 7 analog 18'.
        /// </summary>
        public const string joystick_7_analog_18 = "joystick 7 analog 18";

        /// <summary>
        /// Input axis 'joystick 7 analog 19'.
        /// </summary>
        public const string joystick_7_analog_19 = "joystick 7 analog 19";

        /// <summary>
        /// Input axis 'joystick 8 analog 0'.
        /// </summary>
        public const string joystick_8_analog_0 = "joystick 8 analog 0";

        /// <summary>
        /// Input axis 'joystick 8 analog 1'.
        /// </summary>
        public const string joystick_8_analog_1 = "joystick 8 analog 1";

        /// <summary>
        /// Input axis 'joystick 8 analog 2'.
        /// </summary>
        public const string joystick_8_analog_2 = "joystick 8 analog 2";

        /// <summary>
        /// Input axis 'joystick 8 analog 3'.
        /// </summary>
        public const string joystick_8_analog_3 = "joystick 8 analog 3";

        /// <summary>
        /// Input axis 'joystick 8 analog 4'.
        /// </summary>
        public const string joystick_8_analog_4 = "joystick 8 analog 4";

        /// <summary>
        /// Input axis 'joystick 8 analog 5'.
        /// </summary>
        public const string joystick_8_analog_5 = "joystick 8 analog 5";

        /// <summary>
        /// Input axis 'joystick 8 analog 6'.
        /// </summary>
        public const string joystick_8_analog_6 = "joystick 8 analog 6";

        /// <summary>
        /// Input axis 'joystick 8 analog 7'.
        /// </summary>
        public const string joystick_8_analog_7 = "joystick 8 analog 7";

        /// <summary>
        /// Input axis 'joystick 8 analog 8'.
        /// </summary>
        public const string joystick_8_analog_8 = "joystick 8 analog 8";

        /// <summary>
        /// Input axis 'joystick 8 analog 9'.
        /// </summary>
        public const string joystick_8_analog_9 = "joystick 8 analog 9";

        /// <summary>
        /// Input axis 'joystick 8 analog 10'.
        /// </summary>
        public const string joystick_8_analog_10 = "joystick 8 analog 10";

        /// <summary>
        /// Input axis 'joystick 8 analog 11'.
        /// </summary>
        public const string joystick_8_analog_11 = "joystick 8 analog 11";

        /// <summary>
        /// Input axis 'joystick 8 analog 12'.
        /// </summary>
        public const string joystick_8_analog_12 = "joystick 8 analog 12";

        /// <summary>
        /// Input axis 'joystick 8 analog 13'.
        /// </summary>
        public const string joystick_8_analog_13 = "joystick 8 analog 13";

        /// <summary>
        /// Input axis 'joystick 8 analog 14'.
        /// </summary>
        public const string joystick_8_analog_14 = "joystick 8 analog 14";

        /// <summary>
        /// Input axis 'joystick 8 analog 15'.
        /// </summary>
        public const string joystick_8_analog_15 = "joystick 8 analog 15";

        /// <summary>
        /// Input axis 'joystick 8 analog 16'.
        /// </summary>
        public const string joystick_8_analog_16 = "joystick 8 analog 16";

        /// <summary>
        /// Input axis 'joystick 8 analog 17'.
        /// </summary>
        public const string joystick_8_analog_17 = "joystick 8 analog 17";

        /// <summary>
        /// Input axis 'joystick 8 analog 18'.
        /// </summary>
        public const string joystick_8_analog_18 = "joystick 8 analog 18";

        /// <summary>
        /// Input axis 'joystick 8 analog 19'.
        /// </summary>
        public const string joystick_8_analog_19 = "joystick 8 analog 19";

        /// <summary>
        /// Input axis 'joystick 9 analog 0'.
        /// </summary>
        public const string joystick_9_analog_0 = "joystick 9 analog 0";

        /// <summary>
        /// Input axis 'joystick 9 analog 1'.
        /// </summary>
        public const string joystick_9_analog_1 = "joystick 9 analog 1";

        /// <summary>
        /// Input axis 'joystick 9 analog 2'.
        /// </summary>
        public const string joystick_9_analog_2 = "joystick 9 analog 2";

        /// <summary>
        /// Input axis 'joystick 9 analog 3'.
        /// </summary>
        public const string joystick_9_analog_3 = "joystick 9 analog 3";

        /// <summary>
        /// Input axis 'joystick 9 analog 4'.
        /// </summary>
        public const string joystick_9_analog_4 = "joystick 9 analog 4";

        /// <summary>
        /// Input axis 'joystick 9 analog 5'.
        /// </summary>
        public const string joystick_9_analog_5 = "joystick 9 analog 5";

        /// <summary>
        /// Input axis 'joystick 9 analog 6'.
        /// </summary>
        public const string joystick_9_analog_6 = "joystick 9 analog 6";

        /// <summary>
        /// Input axis 'joystick 9 analog 7'.
        /// </summary>
        public const string joystick_9_analog_7 = "joystick 9 analog 7";

        /// <summary>
        /// Input axis 'joystick 9 analog 8'.
        /// </summary>
        public const string joystick_9_analog_8 = "joystick 9 analog 8";

        /// <summary>
        /// Input axis 'joystick 9 analog 9'.
        /// </summary>
        public const string joystick_9_analog_9 = "joystick 9 analog 9";

        /// <summary>
        /// Input axis 'joystick 9 analog 10'.
        /// </summary>
        public const string joystick_9_analog_10 = "joystick 9 analog 10";

        /// <summary>
        /// Input axis 'joystick 9 analog 11'.
        /// </summary>
        public const string joystick_9_analog_11 = "joystick 9 analog 11";

        /// <summary>
        /// Input axis 'joystick 9 analog 12'.
        /// </summary>
        public const string joystick_9_analog_12 = "joystick 9 analog 12";

        /// <summary>
        /// Input axis 'joystick 9 analog 13'.
        /// </summary>
        public const string joystick_9_analog_13 = "joystick 9 analog 13";

        /// <summary>
        /// Input axis 'joystick 9 analog 14'.
        /// </summary>
        public const string joystick_9_analog_14 = "joystick 9 analog 14";

        /// <summary>
        /// Input axis 'joystick 9 analog 15'.
        /// </summary>
        public const string joystick_9_analog_15 = "joystick 9 analog 15";

        /// <summary>
        /// Input axis 'joystick 9 analog 16'.
        /// </summary>
        public const string joystick_9_analog_16 = "joystick 9 analog 16";

        /// <summary>
        /// Input axis 'joystick 9 analog 17'.
        /// </summary>
        public const string joystick_9_analog_17 = "joystick 9 analog 17";

        /// <summary>
        /// Input axis 'joystick 9 analog 18'.
        /// </summary>
        public const string joystick_9_analog_18 = "joystick 9 analog 18";

        /// <summary>
        /// Input axis 'joystick 9 analog 19'.
        /// </summary>
        public const string joystick_9_analog_19 = "joystick 9 analog 19";

        /// <summary>
        /// Input axis 'joystick 10 analog 0'.
        /// </summary>
        public const string joystick_10_analog_0 = "joystick 10 analog 0";

        /// <summary>
        /// Input axis 'joystick 10 analog 1'.
        /// </summary>
        public const string joystick_10_analog_1 = "joystick 10 analog 1";

        /// <summary>
        /// Input axis 'joystick 10 analog 2'.
        /// </summary>
        public const string joystick_10_analog_2 = "joystick 10 analog 2";

        /// <summary>
        /// Input axis 'joystick 10 analog 3'.
        /// </summary>
        public const string joystick_10_analog_3 = "joystick 10 analog 3";

        /// <summary>
        /// Input axis 'joystick 10 analog 4'.
        /// </summary>
        public const string joystick_10_analog_4 = "joystick 10 analog 4";

        /// <summary>
        /// Input axis 'joystick 10 analog 5'.
        /// </summary>
        public const string joystick_10_analog_5 = "joystick 10 analog 5";

        /// <summary>
        /// Input axis 'joystick 10 analog 6'.
        /// </summary>
        public const string joystick_10_analog_6 = "joystick 10 analog 6";

        /// <summary>
        /// Input axis 'joystick 10 analog 7'.
        /// </summary>
        public const string joystick_10_analog_7 = "joystick 10 analog 7";

        /// <summary>
        /// Input axis 'joystick 10 analog 8'.
        /// </summary>
        public const string joystick_10_analog_8 = "joystick 10 analog 8";

        /// <summary>
        /// Input axis 'joystick 10 analog 9'.
        /// </summary>
        public const string joystick_10_analog_9 = "joystick 10 analog 9";

        /// <summary>
        /// Input axis 'joystick 10 analog 10'.
        /// </summary>
        public const string joystick_10_analog_10 = "joystick 10 analog 10";

        /// <summary>
        /// Input axis 'joystick 10 analog 11'.
        /// </summary>
        public const string joystick_10_analog_11 = "joystick 10 analog 11";

        /// <summary>
        /// Input axis 'joystick 10 analog 12'.
        /// </summary>
        public const string joystick_10_analog_12 = "joystick 10 analog 12";

        /// <summary>
        /// Input axis 'joystick 10 analog 13'.
        /// </summary>
        public const string joystick_10_analog_13 = "joystick 10 analog 13";

        /// <summary>
        /// Input axis 'joystick 10 analog 14'.
        /// </summary>
        public const string joystick_10_analog_14 = "joystick 10 analog 14";

        /// <summary>
        /// Input axis 'joystick 10 analog 15'.
        /// </summary>
        public const string joystick_10_analog_15 = "joystick 10 analog 15";

        /// <summary>
        /// Input axis 'joystick 10 analog 16'.
        /// </summary>
        public const string joystick_10_analog_16 = "joystick 10 analog 16";

        /// <summary>
        /// Input axis 'joystick 10 analog 17'.
        /// </summary>
        public const string joystick_10_analog_17 = "joystick 10 analog 17";

        /// <summary>
        /// Input axis 'joystick 10 analog 18'.
        /// </summary>
        public const string joystick_10_analog_18 = "joystick 10 analog 18";

        /// <summary>
        /// Input axis 'joystick 10 analog 19'.
        /// </summary>
        public const string joystick_10_analog_19 = "joystick 10 analog 19";

        /// <summary>
        /// Input axis 'mouse x'.
        /// </summary>
        public const string mouse_x = "mouse x";

        /// <summary>
        /// Input axis 'mouse y'.
        /// </summary>
        public const string mouse_y = "mouse y";

        /// <summary>
        /// Input axis 'mouse z'.
        /// </summary>
        public const string mouse_z = "mouse z";

        /// <summary>
        /// Input axis 'Horizontal'.
        /// </summary>
        public const string Horizontal = "Horizontal";

        /// <summary>
        /// Input axis 'Vertical'.
        /// </summary>
        public const string Vertical = "Vertical";

        /// <summary>
        /// Input axis 'Fire1'.
        /// </summary>
        public const string Fire1 = "Fire1";

        /// <summary>
        /// Input axis 'Fire2'.
        /// </summary>
        public const string Fire2 = "Fire2";

        /// <summary>
        /// Input axis 'Fire3'.
        /// </summary>
        public const string Fire3 = "Fire3";

        /// <summary>
        /// Input axis 'Jump'.
        /// </summary>
        public const string Jump = "Jump";

        /// <summary>
        /// Input axis 'Mouse X'.
        /// </summary>
        public const string Mouse_X = "Mouse X";

        /// <summary>
        /// Input axis 'Mouse Y'.
        /// </summary>
        public const string Mouse_Y = "Mouse Y";

        /// <summary>
        /// Input axis 'Mouse ScrollWheel'.
        /// </summary>
        public const string Mouse_ScrollWheel = "Mouse ScrollWheel";

        /// <summary>
        /// Input axis 'Submit'.
        /// </summary>
        public const string Submit = "Submit";

        /// <summary>
        /// Input axis 'Cancel'.
        /// </summary>
        public const string Cancel = "Cancel";
    }
}