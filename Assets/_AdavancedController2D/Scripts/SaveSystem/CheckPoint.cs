﻿using System.Collections;
using System.Collections.Generic;
using Com.LuisPedroFonseca.ProCamera2D;
using HrtzzUtilities;
using UnityEditor;
using UnityEngine;

namespace Saya.SaveSystem
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class CheckPoint : MonoBehaviour
    {
        public Transform[] activationObjects;
        public Transform[] deactivationObjects;
        public float transitionDuration = 0.25f;


        private SceneController m_SceneController;
        private BoxCollider2D m_Collider2D;
        private bool m_IsActive = false;

        public int InstanceId { get; private set; }
        public Vector2 Position { get; private set; }

        private void Awake()
        {
            Position = transform.position;
            m_Collider2D = GetComponent<BoxCollider2D>();
            if (m_Collider2D)
            {
                m_Collider2D.isTrigger = true;
            }

            m_SceneController = SceneController.Instance;
            InstanceId = GetInstanceID();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!m_IsActive)
            {
                if (other.CompareTag(Tags.Player))
                {
                    SetSelfAsActiveCheckpoint(transitionDuration);
                }
            }
        }

        public void SetSelfAsActiveCheckpoint(float duration)
        {
            m_IsActive = true;
            StartCoroutine(ActivationRoutine(m_IsActive, duration));
            SceneController.Instance.UpdateCurrentCheckpoint(this);
        }

        public void DeactivateSelf()
        {
            m_IsActive = false;
            StartCoroutine(ActivationRoutine(m_IsActive, 0));
        }

        private IEnumerator ActivationRoutine(bool isActivated, float duration)
        {
            // TODO : Temporary solution, need to be altered for final game.
            for (int i = 0; i < 3; i++)
            {
                if (duration > 0)
                {
                    //ProCamera2DShake.Instance.ShakeUsingPreset("EnemyHit");
                }
                deactivationObjects[i].gameObject.SetActive(!isActivated);
                activationObjects[i].gameObject.SetActive(isActivated);
                yield return new WaitForSeconds(duration);
            }
        }
    }
}