﻿using System.Collections;
using System.Collections.Generic;
using Saya.Controller2D;
using Saya.DamageSystem;
using UnityEngine;

namespace Saya.WeaponSystem
{
    public abstract class Weapon : MonoBehaviour
    {
        [SerializeField] private WeaponType m_WeaponType;

        public WeaponType WeaponType => m_WeaponType;

        public Damager Damager { get; private set; }

        [SerializeField] protected ParticleSystem[] WeaponEffects;

        protected int particleSystemLength;

        protected virtual void Awake()
        {
            Damager = GetComponent<Damager>();
        }

        public abstract void PlayeWeaponEffect();
    }
}