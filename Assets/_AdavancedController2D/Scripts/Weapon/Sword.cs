﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Saya.WeaponSystem
{
    public class Sword : Weapon
    {
        protected override void Awake()
        {
            base.Awake();
            WeaponEffects = GetComponentsInChildren<ParticleSystem>();
            particleSystemLength = WeaponEffects.Length;
        }

        public override void PlayeWeaponEffect()
        {
            ParticleSystem particleSystem = null;
            for (int i = 0; i < particleSystemLength; i++)
            {
                particleSystem = WeaponEffects[i];
                particleSystem.Play(false);
            }
        }
    }
}