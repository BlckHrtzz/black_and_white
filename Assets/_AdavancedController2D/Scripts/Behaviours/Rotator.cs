﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float rotationSpeed = 6;

    public bool isClockwise = true;

    private void Start()
    {
        rotationSpeed *= isClockwise ? -1 : 1;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * rotationSpeed, Space.Self);
    }
}