﻿using System.Diagnostics;

namespace HrtzzUtilities
{
    public static class HrtzzLogger
    {
        // Start is called before the first frame update
        [Conditional("ENABLE_LOGS")]
        public static void Log(string msg)
        {
            UnityEngine.Debug.Log(msg);
        }

        [Conditional("ENABLE_LOGS")]
        public static void LogWarning(string msg)
        {
            UnityEngine.Debug.LogWarning(msg);
        }

        [Conditional("ENABLE_LOGS")]
        public static void LogError(string msg)
        {
            UnityEngine.Debug.LogError(msg);
        }
    }
}