﻿using UnityEngine;
using System.Collections;
using System;

/*
 * Used to allow a later script execution order for FixedUpdate than in GameplayTransform.
 * It is critical this script runs after all other scripts that modify a transform from FixedUpdate.
 */
public class InterpolatedTransformUpdater : MonoBehaviour
{
    InterpolatedTransform m_interpolatedTransform;
    Action UpdateTransform;

    void Awake()
    {
        m_interpolatedTransform = GetComponent<InterpolatedTransform>();

        if (GetComponent<Rigidbody2D>())
        {
            UpdateTransform = PhysicsUpdate;
        }
        else
        {
            UpdateTransform = RegularUpdate;
        }
    }

    void FixedUpdate()
    {
        UpdateTransform?.Invoke();
    }

    void RegularUpdate()
    {
        m_interpolatedTransform.LateFixedUpdate();
    }

    void PhysicsUpdate()
    {
        StartCoroutine(m_interpolatedTransform.LateFixedUpdateRoutine());
    }
}
